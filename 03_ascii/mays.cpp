#include <stdio.h>
#define SALTO ('a' - 'A')


int main () {

    char letra = 'C';

    // si la letra es minuscula //
    if (letra >='a' && letra <='z'){
        letra -= SALTO;
        printf ("letra en mayúsculas: %c\n", letra);
    }
    else if (letra >='A' && letra <='Z'){
        letra += SALTO;
        printf ("letra en minúsculas: %c\n", letra);
    }
    else{
        printf("EL CARACTER DEBE SER UNA LETRA, CAPULLO");
        return 1;
    }


    return 0;
}
