#include <stdio.h>

#define N 20

int main () {
    int num[N];
    int acumulado = 0;

    for (int i=0; i<N; i++)
        num[i] = (i + 1) * (i + 1);
        // Pon en cada celda los cuadrados de los números naturales

        // Usando un bucle y el operador  taquigráfico += guarda en acumulado
        // la suma de todas las celdas
    for (int i=0; i<N; i++)
        acumulado += num[i];



    /* Imprime el resultado */
            printf("La suma de los cuadrados de los numeros naturales del 1 al 20 es: %i \n",acumulado);


    return 0;
}

