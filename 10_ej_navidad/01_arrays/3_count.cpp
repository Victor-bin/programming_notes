#include <stdio.h>
#include <ctype.h>


int main () {
    char nombre[] = "Supercalifragilisticoespialidoso";
    int a, e, i, o, u ,cons;

    a = e = i = o = u = cons =0;

    /* Recorremos las celdas para contar la cantidad de vocales */
    for (int r=0; nombre[r] != '\0'; r++) {

        nombre[r] = tolower(nombre[r]);

        switch (nombre[r]){
            case 'a':
                a++;
                break;
            case 'e':
                e++;
                break;
            case 'i':
                i++;
                break;
            case 'o':
                o++;
                break;
            case 'u':
                u++;
                break;

            default:
                cons++;
        }
    }

    /* Imprime los resultados */

    printf("RECUENTO DE LA PALABRA: %s \n",nombre);
    printf("nº a: %i \n",a);
    printf("nº e: %i \n",e);
    printf("nº i: %i \n",i);
    printf("nº o: %i \n",o);
    printf("nº u: %i \n",u);
    printf("nº Consonantes: %i \n",cons);
    printf("nº total: %i\n", a+e+i+o+u+cons);

    return 0;
}

