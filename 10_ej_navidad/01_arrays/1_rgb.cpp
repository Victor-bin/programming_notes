#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

#define COLS 3
#define R 2
#define G 1
#define B 0

unsigned char pedir_color (const char *color){
	int buffer;
	int letra;

	printf("\n");
	do{
		printf("\x1B[1A");
		printf("Introduce el valor de %s (0-255): ",color);
		printf("          ");
		printf("\x1B[10D");
		__fpurge(stdin);
		scanf(" %u",&buffer);
	}while (buffer < 0 || buffer > 255);

	return (unsigned char) buffer;

}

int main (int argc, char *argv[]) {

	unsigned char rgb[COLS], mask[COLS];
	printf("COLOR:\n");
	printf("======\n");
	rgb[R] = pedir_color("Rojo");
	rgb[G] = pedir_color("Verde");
	rgb[B] = pedir_color("Azul");

	printf("MASCARA:\n");
	printf("========\n");
	mask[R] = pedir_color("Rojo");
	mask[G] = pedir_color("Verde");
	mask[B] = pedir_color("Azul");


	printf("Has introducido : %02X %02X %02X\n",rgb[R] ,rgb[G] ,rgb[B]);
	printf("La mascara es   : %02X %02X %02X\n",mask[R] ,mask[G] ,mask[B]);

	for(int col = B; col < COLS; col++)
	rgb[col] ^= mask[col];

	printf("El resultado es: ");
	for(int col = COLS-1; col >= 0; col--)
	 printf("%02X",rgb[col]);
	printf("\n");


	return EXIT_SUCCESS;
}
