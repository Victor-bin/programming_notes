#include <stdio.h>

const char *nombre[] = {
    "Txema",
    "Teódulo",
    "Teótimo",
    "Telesforo",
    "Teodomiro",
    "Teódulo",
    "Teopisto",
    "Tereso",
    "Tiburcio",
    "Timoteo",
    NULL
};

int main () {
  for (int i=0; nombre[i] ; i++)
      printf("%s es un gran nombre\n",nombre[i]);
}

