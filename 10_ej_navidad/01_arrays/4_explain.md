# parte 2: Arrays, ejercicio 4
## Enunciado
***Explícate a tí mismo por qué al preguntar el valor de nombre no hace falta el ampersand. Házte un dibujo:***

```c
    char nombre[0x20];
    printf ("Nombre: ");
    scanf (" %s", nombre);
```

## datos
- Vemos que se crea una array/cadena de caracteres, es decir una *string* con una longitud de 50.
- Imprimimos "Nombre: " por pantalla
- Asignamos una string de caracteres insertada por teclado a nombre

## Conclusion/explicacion
Sabemos que estamos trabajando con *cadenas*, lo que significa que:
- \*nombre = nombre[  ]
- &nombre = nombre


## Enunciado con punteros

```c
    char *nombre;
    printf ("Nombre: ");
    scanf (" %s", &nombre);
```

