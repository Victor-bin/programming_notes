#include <stdio.h>
#include <stdlib.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};


/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
   al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
    int salida;
    int bit;
    switch (miembro) {
        /* Rellena todos los casos */
        case papa:
            bit = PAPUP ;
            break;
        case mama:
            bit = MAMUP ;
            break;
        case hijo:
            bit =  HIJOP ;
            break;
        case hija:
            bit = HIJAP;
            break;

            salida = old_status ^ bit ;

            /* Realiza la operación a nivel de bits necesaria para
               cambiar el bit correspondiente. Usa los valores en los
               define. No uses if. */
    }
    return salida;
};

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
   al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int salida;

    switch (miembro) {
        /* Rellena todos los casos */
        case papa:
            salida = old_status & ~PAPUP;
            break;
        case mama:
            salida = old_status & ~MAMUP;
            break;
        case hijo:
            salida = old_status & ~HIJOP;
            break;
        case hija:
            salida = old_status & ~HIJAP;
            break;


            /* Realiza la operación a nivel de bits necesaria para
               poner a 0 el bit correspondiente. Usa los valores en los
               define */
    }
    return salida;
};

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
   al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    int salida;

    switch (levantado) {
        /* Rellena todos los casos */
        case papa:
            salida = old_status | PAPUP;
            break;
        case mama:
            salida = old_status | MAMUP;
            break;
        case hijo:
            salida = old_status | HIJOP;
            break;
        case hija:
            salida = old_status | HIJAP;
            break;

            /* Realiza la operación a nivel de bits necesaria para
               poner a 1 el bit correspondiente. Usa los valores en los
               define */

    }
    return salida;
};

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", (waked & PAPUP) == PAPUP  /* Usa una operación de bits*/ ? "levantado": "acostado");
    printf ("Mamá está %s\n", (waked & MAMUP) == MAMUP  /* Usa una operación de bits*/ ? "levantado": "acostado");
    printf ("Hijo está %s\n", (waked & HIJOP) == HIJOP  /* Usa una operación de bits*/ ? "levantado": "acostado");
    printf ("Hija está %s\n", (waked & HIJAP) == HIJAP  /* Usa una operación de bits*/ ? "levantado": "acostado");
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;

    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);

    print (waked);

    return EXIT_SUCCESS;
}

