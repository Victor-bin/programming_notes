#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

void titulo() {
    system("clear");
    system ("toilet --gay -fpagga Colorin colarado");
    return;
}

unsigned char pregunta(const char *componente){
    unsigned int entrada;

    do{
    __fpurge(stdin);
    printf ("intensidad de %s: ", componente);
    scanf(" %u",&entrada);
    } while (entrada > 0xFF);

    return (unsigned char) entrada;
}

int main (int argc, char *argv[]) {
    unsigned char R, G, B;
    unsigned char xorrgb[3];

    titulo();


    R = pregunta ("Rojo");
    G = pregunta ("Verde");
    B = pregunta ("Azul");


    printf("los valores iniciales son: %i %i y %i es decir: #%X%X%X \n", R, G, B, R, G, B);


    xorrgb[0] = R ^ 0xFF;
    xorrgb[1] = G ^ 0xFF;
    xorrgb[2] = B ^ 0xFF;


    printf("#%X%X%X => #%X%X%X\n", R, G, B, xorrgb[0], xorrgb[1], xorrgb[2]);


    return EXIT_SUCCESS;
}
