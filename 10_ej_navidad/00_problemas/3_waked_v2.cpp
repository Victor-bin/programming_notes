#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};

/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
   al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
    int pos = total_miembros - 1 - miembro;

    return old_status ^ (int) pow( 2, pos) ;
};

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
   al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int pos = total_miembros - 1 - miembro;

    return old_status & ~((int) pow( 2, pos)) ;
};

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
   al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    int pos = total_miembros - 1 - levantado;

    return old_status | (int) pow( 2, pos) ;
};

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", waked & PAPUP ? "levantado": "acostado");
    printf ("Mamá está %s\n", waked & MAMUP ? "levantado": "acostado");
    printf ("Hijo está %s\n", waked & HIJOP ? "levantado": "acostado");
    printf ("Hija está %s\n", waked & HIJAP ? "levantado": "acostado");
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;

    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);

    print (waked);

    return EXIT_SUCCESS;
}

