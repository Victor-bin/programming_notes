#include <stdio.h>
#include <stdlib.h>

#define Caracter '*'

void pinta (int nfila, int ncolumna, int ncaracteres){

    if (ncolumna % 2 == 0){
        for (int i = 0; i < ncaracteres; i++){
            if (i == nfila)
                printf("%c",Caracter);

            else
                printf(" ");
        }
    }
    if (ncolumna % 2 == 1){
        for (int i = 0; i < ncaracteres; i++){
            if (i == (ncaracteres - 1) - nfila)
                printf("%c",Caracter);

            else
                printf(" ");
        }
    }

}

int main (int argc, char *argv[]) {

    //Definicion
    int l;

    //Entrada de datos
    printf("¿Cuantas filas pinto?: ");
    scanf(" %i",&l);
    printf("\n");

    //Salida
    for (int f = 0; f < l; f++){
        for (int  i = 0; i < l; i++)
            pinta(f,i,l);



        printf("\n");
    }


    printf("\n");

    return EXIT_SUCCESS;
}
