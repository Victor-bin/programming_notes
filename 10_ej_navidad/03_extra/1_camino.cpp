#include <stdio.h>
#include <stdlib.h>

#define CaracterPar '*'
#define CaracterInpar 'O'

void pinta (int nvuelta, int ncaracteres){
    for (int i = 0; i < ncaracteres; i++){
        if (nvuelta % 2 == 0)
            printf("%c",CaracterPar);

        if (nvuelta % 2 == 1)
            printf("%c",CaracterInpar);

    }

}

int main (int argc, char *argv[]) {

    //Definicion
    int l;


    //Entrada de datos
    printf("¿Cuantas filas pinto?: ");
    scanf(" %i",&l);
    printf("\n");

    //Salida
    for (int p = 0 ;p < l ;p++){
        for (int  i = 0 ;i < l ;i++)
            pinta(i,l);

        printf("\n");
    }


    printf("\n");

    return EXIT_SUCCESS;
}
