#include <stdio.h>
#include <stdlib.h>

#define CaracterPar '*'
#define CaracterInpar 'O'

void pinta (int ncuadrado, int nvuelta, int ncaracteres){
    for (int i = 0; i < ncaracteres; i++){
        if (ncuadrado % 2 == 0){
            if (nvuelta % 2 == 0)
                printf("%c",CaracterPar);

            if (nvuelta % 2 == 1)
                printf("%c",CaracterInpar);

        }

        if (ncuadrado % 2 == 1){
            if (nvuelta % 2 == 0)
                printf("%c",CaracterInpar);

            if (nvuelta % 2 == 1)
                printf("%c",CaracterPar);

        }
    }
}

int main (int argc, char *argv[]) {

    //Definicion
    int l;

    //Entrada de datos
    printf("¿Cuantas filas pinto?: ");
    scanf(" %i",&l);
    printf("\n");

    //Salida
    for (int c = 0 ; c < l;c++)
        for (int f = 0 ;f < l ;f++){
            for (int  i = 0 ;i < l ;i++)
                pinta(c, i, l);

            printf("\n");
        }


    printf("\n");

    return EXIT_SUCCESS;
}
