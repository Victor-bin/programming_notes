#include <stdio.h>
#include <stdlib.h>

enum Colores {
    blanco,
    azul,
    verde,
    cian,
    rojo,
    magenta,
    amarillo,
    negro
};

int main (int argc, char *argv[]) {

    // DEFINICION //
    int color=0;
    char entradar;
    char entradag;
    char entradab;

    // ENTRADA DE DATOS //
    system ("clear");
    printf("\n");

    printf(" ¿Tu color lleva Rojo?  (s/n): ");
    scanf(" %c",&entradar);

    printf(" ¿Tu color lleva Verde? (s/n): ");
    scanf(" %c",&entradag);

    printf(" ¿Tu color lleva Azul?  (s/n): ");
    scanf(" %c",&entradab);

    // Operacion //

    if ( entradar == 's' | entradar == 'S' )
        color += rojo;


    if ( entradag == 's' | entradag == 'S' )
        color += verde;


    if ( entradab == 's' | entradab == 'S' )
        color += azul;


    // Salida //
    switch(color) {
    case blanco:
        printf("\n  Tu color es: Blanco \n");
        break;

    case azul:
        printf("\n  Tu color es: Azul \n");
        break;

    case verde:
        printf("\n  Tu color es: Verde \n");
        break;

    case cian:
        printf("\n  Tu color es: Cian \n");
        break;

    case rojo:
        printf("\n  Tu color es: Rojo \n");
        break;

    case magenta:
        printf("\n  Tu color es: Magenta \n");
        break;

    case amarillo:
        printf("\n  Tu color es: Amarillo \n");
        break;

    case negro:
        printf("\n Tu color es: Negro \n");
        break;

    default :
        printf("has introducido mal los datos");
        return EXIT_FAILURE;
    }
        return EXIT_SUCCESS;
}
