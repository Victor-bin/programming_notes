#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    //Definicion
    int c,f;
    char caracter ='*';

    //Entrada de datos
    printf("¿Cuantos asteriscos pinto?: ");
    scanf(" %i",&c);

    printf("¿Cuantas filas pinto?: ");
    scanf(" %i",&f);
    printf("\n");

    //Salida
    for (int p=0; p<f;p++){
        for (int  i=0; i<c;i++)
            printf("%c",caracter);
        printf("\n");
    }


    printf("\n");

    return EXIT_SUCCESS;
}
