#include <stdio.h>
#include <stdlib.h>


int main (int argc, char *argv[]) {

    // definicion //
    char name[20] ;
    int repeticiones;

    // entrada de datos //
    printf(" ¿Cual es tu nombre?: ");
    fgets(name, 20, stdin);

    printf(" ¿Cuantas repeticiones hago?: ");
    scanf(" %i", &repeticiones);

    for (int i=0 ; i<repeticiones ; i++)
        printf(" Your name: %s  i=%i \n",name ,i+1);


    return EXIT_SUCCESS;
}
