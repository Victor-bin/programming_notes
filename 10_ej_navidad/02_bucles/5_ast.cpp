#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    //Definicion
    int c;
    char caracter ='*';

    //Entrada de datos
    printf("¿Cuantos asteriscos pinto?: ");
    scanf(" %i",&c);

    //Salida
    for (int  i=0; i<c;i++)
        printf("%c",caracter);

    printf("\n");
    return EXIT_SUCCESS;
}
