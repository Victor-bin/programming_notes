#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int main (int argc, char *argv[]) {

    int matriz[MAX + 1][MAX + 1];

    for (int i = 0; i <= MAX; i++){
        for (int n = 0; n <= i; n++){
            if (n != 0 && n != i && i > 1 )
                matriz[i][n] = matriz[i - 1][n] + matriz[i - 1][n - 1];
            else
                matriz[i][n] = 1;
        };
    };



    for (int i = 0; i <= MAX; i++){

        for (int n = 0; n <= MAX - i+1; n++)
            printf("  ");

        for (int n = 0; n <= i; n++)
            printf("%3i ",matriz[i][n]);

        printf("\n");
    };


    return EXIT_SUCCESS;
}
