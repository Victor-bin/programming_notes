#include <stdio.h>
#include <stdlib.h>

#define FIN '\n'


int main (int argc, char *argv[]) {

    char *extrae = NULL;
    char letra;
    int leidos = 0;

    while ((letra = (char) getchar ()) != FIN) {
        extrae = (char *) realloc (extrae, ++leidos * sizeof(char));
        *(extrae + leidos - 1) = letra;

    }

    return EXIT_SUCCESS;
}
