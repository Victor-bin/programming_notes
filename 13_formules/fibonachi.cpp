#include <stdio.h>
#include <stdlib.h>

#define MAX 40

int main (int argc, char *argv[]) {

    unsigned int number[MAX];
    number[0] = number[1] = 1;

    for (int i = 2; i <= MAX; i++)
        number[i] = number[i - 1] + number[i - 2];

    printf("LA FANTASTICA SERIE DE FIBONACCI \n");
    for (int i = 0; i <= MAX; i++)
        printf("%2i. %9u\n", i+1, number[i] );

    return EXIT_SUCCESS;
}
