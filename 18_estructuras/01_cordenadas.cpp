#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct TCordenada{
    double x;
    double y;
};

struct TCordenada preguntar(const char *titulo){
    TCordenada respuesta;
    printf("Datos de %s:\n",titulo);
    printf("\tx:");
    scanf("%lf",&respuesta.x);
    printf("\ty:");
    scanf("%lf",&respuesta.y);
    return respuesta;
}

void pintar(const char *titulo, TCordenada estructura){
    printf("%s\n\
            \t%lf\n\
            \t%lf\n", titulo, estructura.x, estructura.y);
}

int main (int argc, char *argv[]) {
    TCordenada posicion;
    TCordenada velocidad;
    double t;
    clock_t t1 = clock(), t2;

    posicion = preguntar("Posición");
    velocidad = preguntar("Velocidad");

    while(1){
        t2 = clock();
        t = ((double) (t2 - t1)) / CLOCKS_PER_SEC;
        t1 = clock();

        posicion.x += velocidad.x * t;
        posicion.y += velocidad.y * t;

        pintar("Posición",  posicion);
        pintar("Velocidad", velocidad);


        //uselep(1000000);
    }

    return EXIT_SUCCESS;
}
