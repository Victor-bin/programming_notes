#include "main.h"
#include "TEmpleados.h"

int main (int argc, char *argv[]) {
    struct TEmpleado *lista=NULL, *elegido;
    int n;

    for(int i = 0; i < EMP; i++){
        crear(&lista);
        pedir(posicion[i],lista + i);
    }

    do{
        printf("¿Cual es el empleado del mes?\n");
        for( int i = 0; i < EMP; i++ )
            printf("\t%i -> %s %s\n",i + 1, (lista + i) -> nombre, (lista + i) -> ape1);
        scanf("%i",&n);

        if(!(n > 0 && n <= EMP ))
            printf("VALOR INCORECTO\n\n");

    }while(!(n > 0 && n <= EMP ));

    elegido = lista + n - 1;

    elegido -> salario *= 1.01;
    muestra(elegido);

    borrar(lista);

    return EXIT_SUCCESS;
}
