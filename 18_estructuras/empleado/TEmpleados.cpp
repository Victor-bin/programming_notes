#include "TEmpleados.h"

void crear (struct TEmpleado **primero){
    struct TEmpleado *ultimo = *primero;
    struct TEmpleado *nuevo  = (struct TEmpleado *) malloc (sizeof (struct TEmpleado));
    nuevo-> siguiente = NULL;

    if(!ultimo)
        *primero = nuevo;
    return;

    while (ultimo -> siguiente != NULL)
        ultimo = ultimo -> siguiente;
    ultimo -> siguiente = nuevo;
}

void pedir(const char *posicion, struct TEmpleado *consultado){
    printf("Datos del %s empleado:\n",posicion);
    printf("\tNombre: ");
    scanf("%s",consultado -> nombre);
    printf("\t1º Apellido: ");
    scanf("%s",consultado -> ape1);
    printf("\t2º Apellido: ");
    scanf("%s",consultado -> ape2);
    printf("\tEdad: ");
    scanf("%u",&(consultado -> edad));
    printf("\tSalario: ");
    scanf("%lf",&(consultado -> salario));
}

void muestra(struct TEmpleado *empleado){
    printf("Datos del empleado del mes:\n");
    printf("\tNombre: %s\n",empleado -> nombre);
    printf("\t1º Apellido: %s\n",empleado -> ape1);
    printf("\t2º Apellido: %s\n",empleado -> ape2);
    printf("\tEdad: %u\n",(empleado -> edad));
    printf("\tSalario: %lf\n",(empleado -> salario));
}

void borrar(struct TEmpleado *lista){
    if(!lista)
        return;

    if(lista -> siguiente)
        borrar(lista -> siguiente);

    free(lista);

}

