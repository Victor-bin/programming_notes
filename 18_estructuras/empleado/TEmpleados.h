#ifndef __TEMPLEADOS_H_
#define __TEMPLEADOS_H_

#define MAX 0x30

struct TEmpleado {
    char nombre[MAX];
    char ape1[MAX];
    char ape2[MAX];
    unsigned edad;
    double salario;
    struct TEmpleado *siguiente;
};
#ifdef __cplusplus
extern  "C" {
#endif
    void crear (struct TEmpleado **primero);
    void pedir (const char *posicion, struct TEmpleado *consultado);
    void muestra (struct TEmpleado *empleado);
    void borrar (struct TEmpleado *lista);
#ifdef __cplusplus
}
#endif

#include <stdio.h>
#include <stdlib.h>

#endif
