#include <stdio.h>
#include <stdlib.h>

const char *program_name;

const char *nombre[] = {"Cuadrado", "Rectangulo", "Triangulo", NULL};

struct TFigura{
    double ancho;
    double alto;
};

double cuadrado(void *param){
    double lado = *(double *) param;
    return lado * lado;
}

double rectangulo(void *params){
    struct TFigura p = *(TFigura *) params;
    return p.ancho * p.alto;
}

double triangulo(void *params){
    struct TFigura p = *(TFigura *) params;
    return p.ancho * p.alto / 2;
}

void titulo(){
    char salida[128];
    system("clear");
    snprintf(salida, sizeof(salida),"toilet -fpagga --metal %s",program_name + 2);
    system(salida);
    printf("\n\n");
}

int menu(){
    int elegido;
    titulo();
    for(int i = 0; nombre[i]; i++)
        printf("\t%i -> %s\n",i + 1, nombre[i]);
    do{
    printf("\n\nOpcion elegida: ");
    scanf("%i",&elegido);
    } while(elegido > 3 || elegido == 0);
    return elegido - 1;
}

int main (int argc, char *argv[]) {
    double (*operacion[]) (void *) = {&cuadrado, &rectangulo, &triangulo};
    double result;
    void *p;
    double *param = (double *) malloc(sizeof(double));
    struct TFigura *params = (struct TFigura *) malloc(sizeof(struct TFigura));
    int opc;
    program_name = argv[0];
    opc = menu();
    if (opc){
        printf("Introduce la altura: ");
        scanf("%lf",&params -> alto);
        printf("Introduce la anchura: ");
        scanf("%lf",&params -> ancho);
        p = params;
    }
    else{
        printf("Introduce el lado: ");
        scanf("%lf",param);
        p = param;
    }

    result = (operacion[opc]) ((void *) p);

    printf("\n\nEl area de su %s es: %.3lf\n",nombre[opc],result);

    return EXIT_SUCCESS;
}
