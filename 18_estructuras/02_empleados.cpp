#include <stdio.h>
#include <stdlib.h>

#define MAX 0x30

struct TEmpleado {
    char nombre[MAX];
    char ape1[MAX];
    char ape2[MAX];
    unsigned edad;
    double salario;
};

void pedir(const char *posicion, struct TEmpleado *consultado){
    printf("Datos del %s empleado:\n",posicion);
    printf("\tNombre: ");
    scanf("%s",consultado -> nombre);
    printf("\t1º Apellido: ");
    scanf("%s",consultado -> ape1);
    printf("\t2º Apellido: ");
    scanf("%s",consultado -> ape2);
    printf("\tEdad: ");
    scanf("%u",&(consultado -> edad));
    printf("\tSalario: ");
    scanf("%lf",&(consultado -> salario));
}

void muestra(struct TEmpleado *empleado){
    printf("Datos del empleado del mes:\n");
    printf("\tNombre: %s\n",empleado -> nombre);
    printf("\t1º Apellido: %s\n",empleado -> ape1);
    printf("\t2º Apellido: %s\n",empleado -> ape2);
    printf("\tEdad: %u\n",(empleado -> edad));
    printf("\tSalario: %lf\n",(empleado -> salario));
}


int main (int argc, char *argv[]) {
    TEmpleado emp1, emp2, *elegido;
    int n;

    pedir("primer",&emp1);
    pedir("Segundo",&emp2);

    do{
        printf("¿Cual es el empleado del mes?\n\
                \t0 -> %s %s\n\
                \t1 -> %s %s\n",
                emp1.nombre, emp1.ape1,
                emp2.nombre, emp2.ape1);
        scanf("%i",&n);

        if(!(n >= 0 && n < 2 ))
            printf("VALOR INCORECTO\n\n");

    }while(!(n >= 0 && n < 2 ));

    elegido = &emp1 + n;//si n == 1, elegido == emp2

    elegido -> salario *= 1.01;
    muestra(elegido);

    return EXIT_SUCCESS;
}
