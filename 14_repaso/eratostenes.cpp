#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 99
#define INI 2

int main (int argc, char *argv[]) {
    int numero[MAX];

    for (int i = 0; i < MAX;i++)
        numero[i] = i + INI;

    for(int i = 0; pow(i+INI,2) <= MAX; i++)
        if(numero[i] != 0)
            for(int p = 2; numero[i] * p <= MAX + INI; p++)
                numero[numero[i] * p - INI] = 0;

    for(int i = 0; i < MAX; i++)
        if(numero[i] != 0)
            printf("%i es primo\n",numero[i]);


    return EXIT_SUCCESS;
}
