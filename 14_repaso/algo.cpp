#include <stdio.h>
#include <stdlib.h>


int main (int argc, char *argv[]) {
    unsigned  num, dividido, total = 0;

    printf("buenos dias\\tardes\\noches (lo que proceda) querido usuario, necesito un numero \n");
    printf("Numero: ");
    scanf(" %u",&num);
    printf("\n");

    for(int dividido = 1;dividido <= num; dividido++, total = 0){
        for(int i = 1;i <= dividido; i++, total++){
            if (dividido % i == 0 ){
                printf("El numero %3u es divisor exacto de %u\n",i,dividido);
            }
        }
        printf("Total de divisores: %i\n",total);
        if (total < 3)
            printf("¡Oh es primo!\n");
        printf("\n");
    }
    printf("Oh, se nos han acabado los numeros, :( \nNos vemos a la proxima ;)\n");

    return EXIT_SUCCESS;
}
