#include <stdio.h>
#include <stdlib.h>

/* Se solicita un programa que calcule el resultado de elevar num1 por num2    *
 * Es decir: num1 ^ num2                                                       *
 * Las variables num1 y num2 son los datos introducidos                        *
 * La variable salida es el resultado                                          *
 * La variable estado es de cuanto en cuanto se suma en ese momento            *
 *                                                                             *
 * EJ: 4³                                                                      *
 * Estado = 1, salida = 0                                                      *
 * salida(1)  = salida(0)  + estado(1)                                         *
 * salida(2)  = salida(1)  + estado(1)                                         *
 * salida(3)  = salida(2)  + estado(1)                                         *
 * salida(4)  = salida(3)  + estado(1)                                         *
 * Estado     = salida(4)                                                      *
 * salida(8)  = salida(4)  + estado(4)                                         *
 * salida(12) = salida(8)  + estado(4)                                         *
 * salida(16) = salida(12) + estado(4)                                         *
 * Estado     = salida(16)                                                     *
 * salida(32) = salida(16) + estado(16)                                        *
 * salida(48) = salida(32) + estado(16)                                        *
 * salida(64) = salida(48) + estado(16)                                        *
 * Salida = 64                                                                 */

int main (int argc, char *argv[]) {

    int num1, num2, salida = 0, estado = 1;

    // Entrada de datos //
    printf("Introduce la base: \t");
    scanf(" %i",&num1);
    printf("Introduce la potencia: \t");
    scanf(" %i",&num2);

    // Operaciones//
    for(int i = 0; i < num2 ; i++){
        for(int n = 0; i == 0 ? n < num1 : n < num1-1 ; n++)
            salida += estado;
        estado = salida;
    }

    // Salida de datos //
    printf("El resultado de %i elevado a %i es: %i\n",num1,num2,salida);

    return EXIT_SUCCESS;
}
