#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    double *vec1 = NULL, *vec2 = NULL;
    double buffer,  pesca = 0;
    int dim1 = 0, dim2 = 0;
    char end;

    printf("Introduce dos vectores de la misma longitud\n");
    printf("ej: (1.5 2 3.7).\tVector 1: ");

    scanf(" %*[(]");
    do{
        vec1 = (double *) realloc(vec1, (dim1+1) * sizeof(double));
        scanf(" %lf", &buffer);
        vec1[dim1++] = buffer;
    }while (!scanf(" %1[)]", &end));

    buffer=0;

   printf("ej: (1.5 2 3.7).\tVector 2: ");

    scanf(" %*[(]");
    do{
        vec2 = (double *) realloc(vec2, (dim2+1) * sizeof(double));
        scanf(" %lf", &buffer);
        vec2[dim2++] = buffer;
    }while (!scanf(" %1[)]", &end));


    printf("%d y %d\n",dim1,dim2);


    //if (dim1!=dim2){
    //    printf("Las dimensiones no cuadran\n");
    //    free(vec1);
    //    free(vec2);
    //    return EXIT_SUCCESS;
   // }

    for (int i=0; i<dim2; i++)
        pesca += vec1[i] * vec2[i];

    printf("El Producto escalar de tus vectores de dimension %i es: %3.3lf \n",dim2,pesca);

    free(vec1);
    free(vec2);

    return EXIT_SUCCESS;
}
