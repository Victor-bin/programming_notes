#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define X 0
#define Y 1

#define Z 2

#define R 0
#define T 1

#define DIMENSION 3

#define PI 3.14159265

int main (int argc, char *argv[]) {

    double cart[DIMENSION], ciln[DIMENSION];

    //ENTRADA DE DATOS
    for (int i = 0; i < DIMENSION;i++){
        printf("Introduce el %iº valor \n",i+1);
        scanf("%lf",&cart[i]);
    }

    //CALCULOS
    ciln[Z]=cart[Z];
    ciln[T]=atan2(cart[Y],cart[X])* 180 / PI;
    ciln[R]=sqrt(pow(cart[X],2)+pow(cart[Y],2));

    //SALIDA
    printf("Has introducido: X:%.1lf Y:%.1lf Z:%.1lf \n",cart[X],cart[Y],cart[Z]);
    printf("Transfomado en c.cilindricas es: R:%.1lf T:%.1lfº Z:%.1lf \n",ciln[R],ciln[T],ciln[Z]);


    return EXIT_SUCCESS;
}
