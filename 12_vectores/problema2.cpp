#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    double buffer;
    double *vec = NULL;
    static int dim = 0;
    char end;
    double modulo = 0;

    printf("ej: (1.5 2 3.7).\tVector: ");
    scanf(" %*[(]");
    do{
        vec = (double *) realloc(vec, (dim+1) * sizeof(double));
        scanf(" %lf", &buffer);
        vec[dim++] = buffer;
    }while (!scanf(" %1[)]", &end));

    printf("\n\t( ");
    for (int componente=0; componente<dim; componente++)
        printf("%6.2lf", vec[componente]);
    printf(" )\n");


    for (int i=0; i<dim; i++)
        modulo += pow(vec[i],2);
    modulo = sqrt(modulo);


    printf("El modulo de tu vector de dimension %i es: %3.3lf \n",dim,modulo);


    free(vec);



    return EXIT_SUCCESS;
}
