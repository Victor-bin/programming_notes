#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define X 0
#define Y 1

#define Z 2

#define R 0
#define T 1

#define DIMENSION 3

#define PI 3.14159265

int main (int argc, char *argv[]) {

    double cart[DIMENSION], ciln[DIMENSION];

    //ENTRADA DE DATOS
    for (int i = 0; i < DIMENSION;i++){
        printf("Introduce el %iº valor \n",i+1);
        scanf("%lf",&ciln[i]);
    }

    //CALCULOS
    cart[Z]=ciln[Z];
    cart[Y]=ciln[R]*sin(ciln[T] * PI / 180);
    cart[X]=ciln[R]*cos(ciln[T] * PI / 180 );

    //SALIDA
    printf("Has introducido: R:%.1lf T:%.1lfº Z:%.1lf \n",ciln[R],ciln[T],ciln[Z]);
    printf("Transfomado en c.cartesianas es: X:%.1lf Y:%.1lf Z:%.1lf \n",cart[X] ,cart[Y] ,cart[Z]);


    return EXIT_SUCCESS;
}
