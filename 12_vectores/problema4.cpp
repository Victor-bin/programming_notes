#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265
#define X 0
#define Y 1
#define Z 2
#define DIMENSION 3

void PideVector(double vec[DIMENSION]){
  for (int i = 0; i < DIMENSION;i++){
        printf("Introduce el %iº valor \n",i+1);
        scanf("%lf",&vec[i]);
    }
  return;
}



double RadtoDeg (double rad){ return rad * 180 / PI  ;}
double DegtoRad (double deg){ return deg * PI  / 180 ;}

double modulo(double vec[DIMENSION]){
    double salida = 0;
  for (int i=0; i < DIMENSION; i++)
     salida += pow(vec[i],2);
  salida = sqrt(salida);
  return salida;
}

double pescalar(double vec1[DIMENSION],double vec2[DIMENSION]){
    double salida = 0;
  for (int i=0; i < DIMENSION; i++)
      salida += vec1[i] + vec2[i];
  return salida;
}

double angulo_calculo (double escal,double mod1,double mod2){
    double angulo = 0;
    angulo = escal / mod1 * mod2;
    angulo = DegtoRad(angulo);
    angulo = acos(angulo);
    angulo = RadtoDeg(angulo);
    return angulo;
}

int main (int argc, char *argv[]) {

    double vec1[DIMENSION], vec2[DIMENSION],mvec1,mvec2,i,psecal,alpha;

    //ENTRADA DE DATOS
    PideVector(vec1);
    PideVector(vec2);


    //CALCULOS
    mvec1  = modulo(vec1);
    mvec2  = modulo(vec2);
    psecal = pescalar(vec1,vec2);
    alpha  = angulo_calculo(psecal,mvec1,mvec2);

    //SALIDA
    printf("Has introducido: X:%.1lf Y:%.1lf Z:%.1lf \n",vec1[X],vec1[Y],vec1[Z]);
    printf("Su modulo es: %lf\n",mvec1);
    printf("Has introducido: X:%.1lf Y:%.1lf Z:%.1lf \n",vec2[X],vec2[Y],vec2[Z]);
    printf("Su modulo es: %lf\n",mvec2);
    printf("\nEl producto escalar es: %lf\n", psecal);
    printf("El angulo entre ambos es: %lfº\n",alpha);

    return EXIT_SUCCESS;
}
