#ifndef __INTERFAZ_H_
#define __INTERFAZ_H_

#include <stdio.h>
/*
#include <stdlib.h>
*/

#include "pila.h"

#ifdef __cplusplus
extern "C"
{
#endif
    void titulo ();
    int menu ();
    int pedir_pila ();
    void mostrar_pila (struct TPila p);
#ifdef __cplusplus
}
#endif

#endif
