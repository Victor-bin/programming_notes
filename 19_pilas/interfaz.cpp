#include "interfaz.h"

extern const char *program_name;

void titulo () {
    char salida[128];
    system("clear");
    snprintf(salida, sizeof(salida), "toilet -fpagga --metal %s", program_name + 2);
    system(salida);
    printf("\n\n");
}

int menu () {
    int opc;
    do{
        printf("Elije una opcion: \n\
                0 -> Salir\n\
                1 -> Introducir un numero en la pila\n\
                2 -> Sacar un numero de la pila\n");
        scanf("%i",&opc);
    } while(opc < 0 || opc > 2);
    return opc;
}

int pedir_pila (){
    int i;
    titulo();
    printf("Introduce un numero: ");
    scanf("%i",&i);
    return i;
}

void mostrar_pila (struct TPila p) {
    titulo();
    for (int i = p.cima - 1; i >= 0; i--)
        printf("\t%3i\n", p.datos[i]);
    printf("\n\n");
}
