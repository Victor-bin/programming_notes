#include "pila.h"
#include "interfaz.h"

const char *program_name;

int main (int argc, char *argv[]) {
    struct TPila pila;
    int opc = 3;
    program_name = argv[0];

    inicializar(&pila);

    do{
        if (opc == 1)
            guardar(&pila, pedir_pila() );
        if (opc == 2)
            sacar(&pila);
        mostrar_pila(pila);
        opc = menu();
    } while(opc);

    desalojar(&pila);
    return EXIT_SUCCESS;
}
