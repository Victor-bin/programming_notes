#include <stdio.h>

/*
 *DESCRIPCION
 * ASCII Characters table
 */

//function to change screen out to red
void red () {
    printf("\033[0;31m");
};
//function to change screen out to red bold
void red_bold () {
    printf("\033[1;31m");
};
//function to change screen out to normal
void normal () {
    printf("\033[0m");
};


int main () {

    //Definition
    const int column_full = 16;   // This cosnstant define the characters to print in a row.
    const int column_start = 0;   // This constant define the firs character in a row.
    const int ascii_first = 0x20; // This constant define the first ascii to print.
    const int ascii_last = 0x90;  // This constant define the last ascii to print.
    unsigned char ascii;          // This variable is used to increase the loop, and will represent the ASCII number.
    int column = 0;               // This variable is used to count the ASCII characters printed.
    int row = 2 ;                 // This variable is used to count the group of 16 characters printed.

    // Firsts lines
    red();//color red
    printf("ASCII Characters: \n");//First line
    printf("     0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F  \n");
    printf("  -------------------------------------------------- \n");


    //ASCII loop whit screen out
    for (ascii=ascii_first;ascii<ascii_last;ascii++){
        if (column == column_start){// When print the first column
            red_bold();             // Color red bold
            printf("%d | ", row);   // Print file header
        };

        normal();                   // Color normal
        printf(" %c ", ascii);      // Print the character that matches the value of the ascii variable.

        column++;                   // Increment column (because new character is printed).

        if (column == column_full){ // When the last chatacter of this row was printed
            printf("\n");           // change row
            column = 0;             // reset column
            row++;                  // increment row
        };
    };

    return 0;
};
