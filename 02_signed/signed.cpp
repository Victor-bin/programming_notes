#include <stdio.h>

/*
 * This program uses a for loop to display the ASCII characters between h20 and h80.
 */

int main () {

    //Definition
    unsigned char ascii; // this variable is used to increase the loop, and will represent the ASCII number
    const int ascii_first = 0x20; // This constant define the first ascii to print.
    const int ascii_last = 0x80;  // This constant define the last ascii to print.

    // First line
    printf("ASCII Numbers: \n");

    //ASCII loop whit screen out
    for (ascii=ascii_first;ascii<ascii_last;ascii++)
        printf("The character %x in ASCII is: %c\n", ascii, ascii);// x takes it out in hexadecimal and c / n takes it out as an ASCII character

    return 0;
};
