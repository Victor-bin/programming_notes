# Target o fichero de salida
TARGET:=Ejecutable
# Dependencias / ficheros a compilar
DEPS:=$(patsubst %.cpp,%.o,$(wildcard *.cpp))
# Compilador
CC:=g++
# Flags de compilacion y linkado
CFLAGS:=-g
# Librerias externas Ej: ncurses
LDFLAGS:=

# Compilacion
$(TARGET): $(DEPS)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

# Linkado
%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

# Regalas sin necesida de la inteligencia de Make
.PHONY: clean info

# Limpiar los codigos objeto
clean:
	$(RM) *.o

# Mostrar los archivos a compilar
info:
	$(info $(DEPS))
