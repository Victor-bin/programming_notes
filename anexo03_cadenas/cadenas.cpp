#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLIN 0x100

const char * const cprog = "Cadenas";
char vprog[] = "Programa";


/*  Funcion espaciar que:              *
 *    introduce un espacio en la cadena */
void espaciar(char *dest){
    strcat  (dest, " ");  // Concatena " " a el contenido de linea (funcion insegura)
}


/*  Funcion concatena que:                                              *
 *    Avanza en destino hasta \0                                        *
 *    Copia caracter a catacter de org a dest como mucho max o hasta \0 */
void concatena (char *dest, const char *org, int max) {

    while (*dest != '\0') dest++;
    /* versiones alrernativas:          *
     *  while (*dest(dest++) != '\0');  *
     *  for (;*dest != '\0'; dest++);   */


    for (int c=0; *org!='\0' && c<max; c++, org++, dest++) //usamos c++ o ++c indistintamente
        *dest = *org;
    *dest = *org;

}

int main (int argc, char *argv[]) {

    char linea[MAXLIN];
    strcpy  (linea, cprog);                         // Copia cprog hasta el /0 (funcion insegura)
    strncpy (linea, cprog, MAXLIN);                 // Copia cprog la MAXLIN caracteres a linea
    espaciar(linea);
    strncat (linea, vprog, MAXLIN - strlen(linea)); // Concatena el contenido de vprog al de linea
    espaciar(linea);
    concatena(linea, vprog, MAXLIN - strlen(linea));

        printf  ("%s\n", linea);

    return EXIT_SUCCESS;
}
