#include <stdio.h>
#include <stdlib.h>

double funcion(double dato) {  	// El dato externo es la x de f(x)
	return dato * dato - 3; 	// f(x)=x*x-3
}

int main (int argc, char *argv[]) {

    for (double i=-5; i<5; i+=0.5)   //for que recorre de -5 a 5 de 0.5 en 0.5
        printf ("x: %.2lf, f(x): %.2lf\n", i, funcion(i)); //pintas el punto y llamas a la funcion funcion

    return EXIT_SUCCESS;
}
