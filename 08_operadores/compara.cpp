#include <stdio.h>
#include <stdlib.h>

#define MAX_ERROR .01

int main (int argc, char *argv[]) {

    double user_number;

    printf ("Number: ");
    scanf (" %lf", &user_number);
    printf ("\n");

    if (user_number >= 3. - MAX_ERROR &&
        user_number <= 3. + MAX_ERROR)
        printf ("Para mi es un 3 \n");
    else
        printf ("No es un 3 muyayo \n");

    return EXIT_SUCCESS;
}
