#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    /* Definicion de datos*/
    char respuesta;
    int  num1,num2,result;

    /* Bloque de entrada de datos */
    printf("Que operacion hago? (s/r): ");
    scanf(" %c",&respuesta);

    if ( respuesta != 's' && respuesta != 'S' && respuesta != 'r' && respuesta != 'R'){
        fprintf(stderr,"Procedo a ignorarte, %c no es una opcion valida \n",respuesta);
        return EXIT_FAILURE;
    }

    printf("Introduce el primer numero a operar: ");
    scanf(" %i",&num1);

    printf("Introduce el segundo numero a operar: ");
    scanf(" %i",&num2);

    /* Bloque de operacion */
    if ( respuesta == 's' || respuesta == 'S')
        result= num1 + num2;

    if ( respuesta == 'r' || respuesta == 'R')
        result= num1 - num2;


    /* Bloque de salida */
    printf("Procedo a operar, \n el resultado es : %i \n",num1 - num2);

    return EXIT_SUCCESS;
}
