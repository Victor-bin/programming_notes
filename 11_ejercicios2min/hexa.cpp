#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

int main (int argc, char *argv[]) {

    char leido[MAX];
    int numero;

    printf("Introduce un numero en Hexadecimal: ");
    scanf("%[0-9a-fA-F]",leido);
    numero = atoi (leido);

    printf("HAS INTRODUCIDO: %i\n",numero);

    return EXIT_SUCCESS;
}
