#include <stdio.h>
#include <stdlib.h>

#define standar_phrase 20

const char *palabras[] = {
    "H0l4 t10",
    "Bu5n4s N0ch5s",
    NULL
};

int main (int argc, char *argv[]) {

    char frase[standar_phrase];

    printf("%s ¿En que estas pensando?\n",palabras[0]);
    scanf(" %[^\n]",frase);
    printf("%s\n",frase);

    printf("Bytes de tu lista : %lu \n",sizeof(palabras) );
    printf("Bytes por celda   : %lu \n",sizeof(*palabras));
    printf("Total de celda    : %lu \n",sizeof(palabras) / sizeof(*palabras));
    //unsigned long nceldas = sizeof(palabras) / sizeof (*palabras);

    const char **p = palabras;
//    for(int i=0; i < nceldas; i++ )
//        printf("celda numero %i: %s\n",i,palabras[i]);
      while(*p != NULL){
          printf("%s\n",*p);
          p++;
      }

      printf("\n");

    /* bytes, bite de cada celda, total de celdas, imprimir todas las palabras*/
    return EXIT_SUCCESS;
}
