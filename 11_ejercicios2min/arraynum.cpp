#include <stdio.h>
#include <stdlib.h>

#define MAX 10
#define LETRA 'u'

int main (int argc, char *argv[]) {

    char frase[]= "La luna alumbraba la noche", *celdas[MAX], totaletra=0;
    int posiciones[MAX];

    for (int i = 0; frase[i] != '\0'; i++){
        if (frase[i] == LETRA){
        celdas[totaletra]     = &frase[i];
        posiciones[totaletra] = i;
        totaletra++;
        };
    };

    printf("En la frase: %s.\nHay %i letras:%c en total.\n Sus posiciones son:\n\n",frase ,totaletra, LETRA);

    for(int i = 0; i < totaletra; i++)
    printf("%i. celda:%p, posicion:%i\n",i ,celdas[i],posiciones[i] );


    return EXIT_SUCCESS;
}
