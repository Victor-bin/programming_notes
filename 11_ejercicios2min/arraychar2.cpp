#include <stdio.h>
#include <stdlib.h>

#define FANTE 20

int main (int argc, char *argv[]) {

    unsigned char ele[FANTE];

    for(int i = 0; i < FANTE; i++)
        ele[i] = (unsigned char) ((i + 1) * (i + 1));


    for(int i = 0; i < FANTE; i++)
        printf("%2i elefante(s) se balanceaba sobre la tela de una araña y como veian que no se caia(n) fueron a llamar a otro elefante \n",ele[i]);


    return EXIT_SUCCESS;
}
