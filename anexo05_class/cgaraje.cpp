#include "cgaraje.h"
#include <string.h>
#include <cstdlib>
#include <cstdio>

CGaraje::CGaraje (int pisos, int huecos, char *estado, char *direccion) {
    this->pisos = pisos;
    this->huecos = huecos;
    strcpy(this->estado, estado);
    strcpy(this->direccion, direccion);
    this->plaza = (bool *) malloc(pisos * huecos * sizeof(bool));
}

void CGaraje::busca_sitio(){
    for( int p = 0; p < this->pisos; p++ )
        for( int h = 0; h < this->huecos; h++  )
            if( *(this->plaza + h + p * this->huecos) != OCUPADO){
                printf("La plaza piso:%i hueco:%i esta libre\n",p,h);
                return;
            }
    printf("NO hay sitios disponinbles\n");
    strcpy(this->estado, "Lleno");
}

void CGaraje::aparca(int piso, int hueco){
    bool *aparcamiento = this->plaza + hueco%this->huecos + (piso % this->pisos * this->huecos);
    if( *aparcamiento == OCUPADO){
        printf("Plaza ya ocupada, pruebe otro sitio\n");
    }
    else
        *aparcamiento = OCUPADO;
}

void CGaraje::desaparca(int piso, int hueco){
    bool *aparcamiento = this->plaza + hueco%this->huecos + (piso % this->pisos * this->huecos);
    if( *aparcamiento != OCUPADO)
        printf("Plaza no ocupada, pruebe otro sitio\n");
    else
        *aparcamiento = LIBRE;
}
