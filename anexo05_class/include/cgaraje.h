#ifndef __CGARAJE_H__
#define __CGARAJE_H__

#define MAX 0x100
#define OCUPADO 1
#define LIBRE 0

class CGaraje {
    bool *plaza;
    int pisos;
    int huecos;
    char estado[MAX];
    char direccion[MAX];

    public:

    CGaraje() = delete;
    CGaraje(int pisos, int huecos, char *estado, char *direccion);
    void busca_sitio();
    void aparca(int piso, int hueco);
    void desaparca(int piso, int hueco);
};

#endif
