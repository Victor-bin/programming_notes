#include <stdio.h>

int main () {

    // Char variable
    char var_char;

    // Float variable
    float var_float;

    // Double variables
    double var_double;
    long double var_long_double;

    // Int variables
    int var_int;
    short int var_short_int;
    long int var_long_int;
    long long int var_long_long_int;

    printf("CHAR DATAS: \n");
    printf("char: %lu bytes. \n", sizeof (var_char));
    printf("\n");

    printf("FLOAT DATAS: \n");
    printf("float: %lu bytes. \n", sizeof (var_float));
    printf("\n");

    printf("DOUBLE DATAS: \n");
    printf("double: %lu bytes. \n", sizeof (var_double));
    printf("long double: %lu bytes \n", sizeof (var_long_double));
    printf("\n");

    printf("INT DATAS: \n");
    printf("int: %lu bytes. \n", sizeof (var_int));
    printf("short int: %lu bytes. \n", sizeof (var_short_int));
    printf("long int: %lu bytes. \n", sizeof (var_long_int));
    printf("long long int: %lu bytes.", sizeof (var_long_long_int));
    printf("\n");

    return 0;

}
