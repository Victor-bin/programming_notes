#ifndef __COLA_H_
#define __COLA_H_

#include <stdlib.h>

#define SZ 8

struct TCola {
    int datos[SZ];
    int cima;
    int inicio;
};

#ifdef __cplusplus
extern "C"
{
#endif
    void inicializar (struct TCola *c);
    void guardar (struct TCola *c, int dato);
    int    sacar (struct TCola *c);
#ifdef __cplusplus
}
#endif

#endif
