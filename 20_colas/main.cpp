#include "cola.h"
#include "interfaz.h"

const char *program_name;

int main (int argc, char *argv[]) {
    struct TCola cola;
    int opc = 3;
    program_name = argv[0];

    inicializar(&cola);

    do{
        if (opc == 1)
            guardar(&cola, pedir_cola() );
        if (opc == 2)
            sacar(&cola);
        mostrar_cola(cola);
        opc = menu();
    } while(opc);

    return EXIT_SUCCESS;
}
