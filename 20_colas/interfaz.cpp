#include "interfaz.h"

extern const char *program_name;

void titulo () {
    char salida[128];
    system("clear");
    snprintf(salida, sizeof(salida), "toilet -fpagga --metal %s", program_name + 2);
    system(salida);
    printf("\n\n");
}

int menu () {
    int opc;
    do{
        printf("Elije una opcion: \n\
                0 -> Salir\n\
                1 -> Introducir un numero\n\
                2 -> Sacar un numero\n");
        scanf("%i",&opc);
    } while(opc < 0 || opc > 2);
    return opc;
}

int pedir_cola (){
    int i;
    titulo();
    printf("Introduce un numero: ");
    scanf("%i",&i);
    return i;
}

void mostrar_cola (struct TCola c) {
    titulo();
    for (int i = c.inicio  ; i <= c.cima - 1; i++)
        printf("\t%3i\n", c.datos[i]);
    printf("\n\n");
}
