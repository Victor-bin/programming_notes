#ifndef __INTERFAZ_H_
#define __INTERFAZ_H_

#include <stdio.h>
#include "cola.h"

#ifdef __cplusplus
extern "C"
{
#endif
    void titulo ();
    int menu ();
    int pedir_cola ();
    void mostrar_cola (struct TCola c);
#ifdef __cplusplus
}
#endif

#endif
