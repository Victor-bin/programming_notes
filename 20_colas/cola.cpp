#include "cola.h"

void inicializar (struct TCola *c){
    c -> cima = 0;
    c -> inicio = 0;
}

void guardar (struct TCola *c, int dato){
    if(c -> cima - c -> inicio < SZ)
    c -> datos[c -> cima++ % SZ] = dato;
}

int sacar (struct TCola *c){
    if(c -> cima <= c -> inicio)
        return -404;
    return c -> datos[c -> inicio++ % SZ];
}
