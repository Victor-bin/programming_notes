#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

#define R   2
#define COL 4
#define ROW 3

void pedir_datos (bool *m, int filas, int cols) {
    int temp;
    printw("Introduce los datos de la Matriz (unos y ceros), utiliza el espacio para salir");
    printw("\n");

    for (int f=0; f<filas; f++) {
        for (int c=0; c<cols; c++) {
            printw("  ");
            mvscanw(f + 1,c *2,"%i", &temp);
            *( m + f * cols + c) = temp;
        }
        printw("\n");
    }
}


void computa(bool *arr, int row, int col, unsigned *contarr){
    for (int y = 0; y < row / ROW; y++)
        for (int x = 0; x < col / COL; x++)
            if (*(arr + (y * row / ROW + x))){
                if ( !( *(contarr + (y * row / ROW + x)) >= 2 && *(contarr + (y * row / ROW + x )) <= 3 ))
                    *(arr + (y * row / ROW + x)) = 0;
            }
            else
                if (*(contarr + (y * row / ROW + x)) == 3)
                    *(arr + (y * row / ROW + x)) = 1;
}

void rellenaarray(bool *arr, int row, int col){
    srand(time(NULL));
    for (int y = 0; y < row / ROW; y++)
        for (int x = 0; x < col / COL; x++)
            *(arr + (y * row / ROW + x)) = rand() % R;
}

void cuentavivas(bool *arr, int y, int x, int row, int col, unsigned *contarr){
    int y_limi = -1, y_lims = 2, x_limi = -1, x_lims = 2,total = 0;
    if( y == 0 ) y_limi++;
    if( y == row / ROW ) y_lims--;
    if( x == 0 ) x_limi++;
    if( x == col / COL) x_lims--;

    for (int n = y_limi; n < y_lims; n++)
        for (int m = x_limi; m < x_lims; m++)
            total += *(arr + ((y+n) * row / ROW + (x+m)));
    total -= *(arr + (y * row / ROW + x));
    *(contarr + (y * row / ROW + x)) = total;;

}

void creacelda(int y, int x, bool llena){
    if(llena){
        mvaddch(y * ROW,     x * COL, ACS_ULCORNER); addch(ACS_HLINE  ); addch(ACS_HLINE  ); addch(ACS_URCORNER);
        mvaddch(y * ROW + 1, x * COL, ACS_VLINE   ); addch(ACS_CKBOARD); addch(ACS_CKBOARD); addch(ACS_VLINE   );
        mvaddch(y * ROW + 2, x * COL, ACS_LLCORNER); addch(ACS_HLINE  ); addch(ACS_HLINE  ); addch(ACS_LRCORNER);
    }
    else{
        mvaddch(y * ROW,    x * COL, ACS_ULCORNER); addch(ACS_HLINE);  addch(ACS_HLINE); addch(ACS_URCORNER);
        mvaddch(y * ROW + 1,x * COL, ACS_VLINE     ); printw(" "     );  printw(" "     ); addch(ACS_VLINE   );
        mvaddch(y * ROW + 2,x * COL, ACS_LLCORNER  ); addch(ACS_HLINE);  addch(ACS_HLINE); addch(ACS_LRCORNER);
    }
}

int main()
{
    int row, col;
    bool *arr;
    unsigned *contarr;
    char ch;
    initscr();
    getmaxyx(stdscr, row, col);
    arr =     (bool *)     malloc( row / ROW * col / COL * sizeof(bool));
    contarr = (unsigned *) malloc( row / ROW * col / COL * sizeof(unsigned));
    memset(arr, 0, row / ROW * col / COL * sizeof(bool));
    memset(contarr, 0, row / ROW * col / COL * sizeof(unsigned));
    rellenaarray(arr , row, col);
    pedir_datos (arr, row / ROW, col / COL);

    do{
        for (int y = 0; y < row / ROW; y++)
            for (int x = 0; x < col / COL; x++){
                creacelda(y,x, *(arr + (y * row / ROW + x)));
                cuentavivas(arr, y, x, row, col,contarr);
            }

        refresh();
        ch = getch();
        computa(arr, row, col,contarr);

    }while(ch != ' ');
    // ESTO SOLO PAra PRUEBAS INTERNAS
    //FILE* fichero = fopen("out.p","wt");
    //for (int y = 0; y < row / ROW; y++){
    //    for (int x = 0; x < col / COL; x++)
    //        fprintf(fichero, "%u\t ",*(contarr + (y * row / ROW + x)));
    //    fprintf(fichero, "\n");
    //}
    endwin();
        for (int y = 0; y < row / ROW; y++)
            for (int x = 0; x < col / COL; x++){
                free(arr + y * row /ROW + x);
                free(contarr + y * row /ROW + x);
            }
    return 0;
}
