#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D 3 /* D: Dimensión */
#define V 3 /* V: Número de vectores */

double multiplicacion(int i, int j, double a[V][D], double b[V][D]){

    double mult = 0;

    for (int k=0; k<D;k++)
        mult += a[i][k] * b[k][j];

    return mult;
}
int main(int argc, char *argv[])
{
    double a[V][D] = {
        { 1 , 2 , 3 },
        { 2 , 3 , 4 },
        {-1 , 5 ,-4 }},
           b[V][D] = {
        { 1 , 2 , 3 },
        { 2 , 3 , 4 },
        {-1 , 5 ,-4 }},
           c[V][D];

    for (int i = 0; i < V; i++)
        for (int j = 0; j < D ;j++)
            c[i][j] = multiplicacion(i,j,a,b);
    for (int i = 0; i < V; i++){
        for (int j = 0; j < D ;j++)
            printf("%2.2lf ",c[i][j]);

        printf("\n");
   }


    return EXIT_SUCCESS;
}
