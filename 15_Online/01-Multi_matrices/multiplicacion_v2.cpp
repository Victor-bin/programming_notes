#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D 2 /* D: Dimensión */
#define V 3 /* V: Número de vectores */
#define S 4 /* S: Numero a sumar*/

double multiplicacion(int i, int j, double a[D][S], double b[S][V]){

    double mult = 0;

    for (int k=0; k<S;k++)
        mult += a[i][k] * b[k][j];

    return mult;
}
int main(int argc, char *argv[])
{
    double a[D][S] = {
        { 1 , 2 , 3 , 4 },
        {-1 , 5 ,-4 , 3 }},
           b[S][V] = {
        { 1 , 2 , 3 },
        { 2 , 3 , 4 },
        {-1 , 5 ,-4 },
        { 5 , 6 , 7 }},
           c[D][V];

    for (int i = 0; i < D; i++)
        for (int j = 0; j < V ;j++)
            c[i][j] = multiplicacion(i,j,a,b);

    for (int i = 0; i < D; i++){
        for (int j = 0; j < V ;j++)
            printf("%2.2lf ",c[i][j]);

        printf("\n");
   }


    return EXIT_SUCCESS;
}
