#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void pedir_datos (double *m, const char *label, int filas, int cols) {
    printf ("Introduce los datos de la Matriz %s\n", label);
    printf ("\n");

    for (int f=0; f<filas; f++) {
        for (int c=0; c<cols; c++) {
            printf ("\t\t\t");
            for (int tab=0; tab<c; tab++)
                printf ("\t");
            scanf ("%lf", m + f * cols + c);
            printf("\x1b[%iA", 1);
        }
        printf ("\n");
    }
}

double * crear(unsigned f, unsigned c){
    return (double *) malloc ( f * c * sizeof(double) );
}

int main(int argc, char *argv[]){

    unsigned D,V,S;
    printf("introduce la m (matriz m*k): ");
    scanf("%u", &D);
    printf("\nintroduce la k (matriz m*k): ");
    scanf("%u", &S);
    printf("introduce la n (matriz k*n: ");
    scanf("%u", &V);



    double *a, *b, *c;
  //  a = (double *) malloc ( D * S * sizeof(double) );
  //  b = (double *) malloc ( S * V * sizeof(double) );
  //  c = (double *) malloc ( D * V * sizeof(double) );
      a = crear(D,S);
      b = crear(S,V);
      c = crear(D,V);

    pedir_datos ((double *) a ,"A",D,S);
    pedir_datos ((double *) b ,"B",S,V);

    for (int i = 0; i < D; i++)
        for (int j = 0; j < V ; j++)
            for (int k = 0; k < S; k++)
                *(c + i * V + j) += *(a + i * S + k) * *(b + k * V + j);

    for (int i = 0; i < D; i++){
        for (int j = 0; j < V ;j++)
            printf("%2.2lf ",*(c + i * V + j));

        printf("\n");
    }
    free(a);
    free(b);
    free(c);


    return EXIT_SUCCESS;
}
