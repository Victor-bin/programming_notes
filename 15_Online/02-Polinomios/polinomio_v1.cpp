#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f(double *pol, int grado, double x){
    double valor = 0;

    for(int i = 0; i <= grado; i++)
        valor += *(pol + i) * pow(x,i);


    return valor;
}


int main (int argc, char *argv[]) {
    double pol[] = {-3, 0, 4, 3};
    int g = sizeof(pol) / sizeof(double) - 1;

    printf("El valor de el polinomio con x = 1 es: %.2lf\n", f(pol,g,1));
    printf("El valor de el polinomio con x = 2 es: %.2lf\n", f(pol,g,2));
    printf("El valor de el polinomio con x = 3 es: %.2lf\n", f(pol,g,3));


    return EXIT_SUCCESS;
}
