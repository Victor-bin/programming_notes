#include <stdio.h>
#include <stdlib.h>
#include <math.h>

unsigned pregunta_grado(){
    unsigned g;
    printf("¿De que grado es el polinomio que va a intoducir?  \t");
    scanf(" %u",&g);
    return g;
}

void pregunta_limite(double *li,double *ls, double *inc){
    printf("¿Cual es el limite inferior?\t");
    scanf(" %lf",li);
    printf("¿Cual es el limite superior?\t");
    scanf(" %lf",ls);
    printf("¿Cual es el incremento?\t");
    scanf(" %lf",inc);


}

double * crear(unsigned grado){
    return (double *) malloc( (grado + 1) * sizeof(double) );
}

void pregunta_polinomio(double *pol,unsigned  grado){
    printf("Introduce los datos del polinomio de grado %i (de forma ascendente)\n",grado);

    for (int i = 0; i <= grado; i++){
        for (int j = 0; j <= i; j++)
          printf("\t");

        scanf(" %lf",pol + i);
        printf("\x1b[%iA",1);
    }
    printf("\n");

    return;
}


double f(double *pol, unsigned grado, double x){
    double valor = 0;

    for(int i = 0; i <= grado; i++)
        valor += *(pol + i) * pow(x,i);


    return valor;
}

bool zero(double *pol, unsigned grado, double li, double ls){
    double max, min;

    max = f(pol, grado, ls);
    min = f(pol, grado, li);

    if(max >= 0 && min >= 0)
        return true;

    if(max <  0 && min <  0)
        return true;

    return false;
}

int main (int argc, char *argv[]) {
    //double pol[] = {-3, 0, 4, 3};
    //unsigned g = sizeof(pol) / sizeof(double) - 1;
    double *pol, li, ls, inc;
    unsigned g = pregunta_grado();
    pregunta_limite(&li, &ls, &inc);
    pol = crear(g);
    pregunta_polinomio(pol, g);

    for(double x = li; x <= ls; x += inc)
    printf("El valor de el polinomio con x = %.2lf es: %.2lf\n", x, f(pol, g, x) );

    printf(zero(pol, g, li, ls) ? "Signos iguales\n": "Signos distintos\n");

    free(pol);

    return EXIT_SUCCESS;
}
