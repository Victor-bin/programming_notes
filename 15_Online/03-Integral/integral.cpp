#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define W 1

unsigned pregunta_grado(){
    unsigned g;
    printf("¿De que grado es el polinomio que va a intoducir?  \t");
    scanf(" %u",&g);
    return g;
}

void pregunta_limite(double *li,double *ls){
    printf("¿Cual es el limite inferior?\t");
    scanf(" %lf",li);
    printf("¿Cual es el limite superior?\t");
    scanf(" %lf",ls);
}

double * crear(unsigned grado){
    return (double *) malloc( (grado + 1) * sizeof(double) );
}

void pregunta_polinomio(double *pol,unsigned  grado){
    printf("Introduce los datos del polinomio de grado %i (de forma ascendente)\n",grado);

    for (int i = 0; i <= grado; i++){
        for (int j = 0; j <= i; j++)
            printf("\t");

        scanf(" %lf",pol + i);
        printf("\x1b[%iA",1);
    }
    printf("\n");
    return;
}

double f(double *pol, unsigned grado, double x){
    double valor = 0;

    for(int i = 0; i <= grado; i++)
        valor += *(pol + i) * pow(x,i);

    return valor;
}

double integral(double *pol, unsigned grado, double li, double ls){
    double inte = 0;

    for(int x = li; x <= ls; x++)
        inte += f(pol, grado, x);
    inte *= W;

    return inte;
}

int main (int argc, char *argv[]) {
    double *pol, li, ls, inte;
    unsigned g = pregunta_grado();
    pol = crear(g);
    pregunta_polinomio(pol, g);
    pregunta_limite(&li, &ls);

    inte = integral(pol, g, li, ls);
    printf("La integral del polinomio es: %.2lf\n", inte);

    free(pol);

    return EXIT_SUCCESS;
}
