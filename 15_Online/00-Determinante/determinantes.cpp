#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D 3 /* D: Dimensión */
#define V 3 /* V: Número de vectores */


double determinantes(double op1[V][D]) {

    double producto, diagonal1 = 0, diagonal2 = 0, asc = 1, des = 1;

    for(int d = 0; d<D; d++, asc = des = 1){
        for(int i=d, n=0; n<D; i++,n++)
            asc *= op1[i%V][n%V];

        for (int i=d,n=2; i<D+d; i++,n+=2)
            des *= op1[i%V][n%V];

        diagonal1 += asc;
        diagonal2 += des;
    }

    producto = diagonal1 - diagonal2;

    return producto;
}
int main(int argc, char *argv[])
{
    double v[V][D] = {
        { 1 , 2 , 3 },
        { 2 , 3 , 4 },
        {-1 , 5 ,-4 }};

    printf("%lf\n",determinantes(v));
    return EXIT_SUCCESS;
}
