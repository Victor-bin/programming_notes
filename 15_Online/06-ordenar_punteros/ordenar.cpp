#include <stdio.h>
#include <stdlib.h>

void ordenar(int dim , int **arr){
    int *tmp;

    for(int i = 0; i < dim ; i++)
        for(int n = 0; n < dim - 1; n++)
            if (**(arr + n) > **(arr + n + 1)){
                tmp = *(arr + n +1);
                *(arr + n + 1) = *( arr + n );
                *(arr + n) = tmp;
            }
    return;
}

int main (int argc, char *argv[]) {
    int dim = 6;
    int arrnum[dim] = {3,1,4,2,5,6};
    int *arrpoint[dim];

    for (int i = 0; i < dim; i++)
        arrpoint[i] =  &arrnum[i];


    ordenar(dim,arrpoint);

    printf("hay %i numeros validos\n",dim);
    for( int i = 0; i < dim; i++)
        printf("\t%.i\n",arrnum[i]);

    printf("y ordenados son:\n");
    for(int i = 0; i < dim; i++)
        printf("\t%.i\n",*arrpoint[i]);


    return EXIT_SUCCESS;
}
