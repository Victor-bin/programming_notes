#include <stdio.h>
#include <stdlib.h>

double *pedir(double *arr, int *dim){
    double buffer;

    do{
        scanf("%lf",&buffer);
        if( buffer >= 0){
            arr = (double *) realloc(arr, ++(*dim) * sizeof(double));
            *(arr + *dim - 1) = buffer;
        }
    }while(buffer >= 0);

    return arr;
}

void ordenar(double *arr, int dim){
    int tmp;

    for(int n = 0; n < dim - 1; n++)
        if (*(arr + n) > *(arr + n + 1)){
            tmp = *(arr + n +1);
            *(arr + n + 1) = *( arr + n );
            *(arr + n) = tmp;

            for(int i = n - 1; i >= 0; i--)
                if (*(arr + i) > tmp){
                    *(arr + i + 1) = *(arr + i);
                    *(arr + i) = tmp;
                }
                else{
                    *(arr + i + 1) = tmp;
                    tmp = *(arr + i);
                }
        }
    return;
}

double mediana(double *arr, int dim){
    double result = 0;
    if (dim % 2)
         result = *(arr + dim / 2 );
    else{
        result += *(arr + dim / 2);
        result += *(arr + dim / 2 - 1);
        result /= 2;
    }
    return result;
}

int main (int argc, char *argv[]) {
    double *arr = NULL;
    int dim = 0;

    arr = pedir(arr,&dim);
    ordenar(arr,dim);

    printf("has introducido %i numeros validos\n",dim);
    printf("y ordenados son:\n");
    for(int i = 0; i < dim; i++)
        printf("\t%.2lf\n",*(arr + i));
    printf("cuya mediana es: %.2lf\n",mediana(arr, dim));

    free(arr);

    return EXIT_SUCCESS;
}
