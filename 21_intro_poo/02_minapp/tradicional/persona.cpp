#include "persona.h"
#include <string.h>
#include <stdio.h>

void init(TPersona *p, unsigned edad, const char *nombre){
    p->edad = edad;
    strcpy(p->nombre, nombre);
}

void display(const TPersona *p) {
    printf("Nombre: %s\nEdad: %u\n", p->nombre, p->edad);
}
