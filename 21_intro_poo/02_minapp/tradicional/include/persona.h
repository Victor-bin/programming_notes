#ifndef __PERSONA_H__
#define __PERSONA_H__

#define MAX 0x100

struct TPersona{
    unsigned edad;
    char nombre[MAX];
};
/* C exported functions */
#ifdef __cplusplus
extern "C"
{
#endif
   void init(TPersona *p, unsigned edad, const char *nombre);
   void display(const TPersona *p);
#ifdef __cplusplus
}
#endif

#endif
