#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "persona.h"

int main (int argc, char *argv[]) {

    TPersona persona;

    init(&persona, 34, "pepe");

    display(&persona);

    return EXIT_SUCCESS;
}
