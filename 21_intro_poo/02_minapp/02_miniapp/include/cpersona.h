#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 0x100

class CPersona {
    unsigned edad;
    char nombre[MAX];

    public:

    CPersona() = delete;
    CPersona(unsigned edad, const char *nombre);
    void display ();
};

#endif
