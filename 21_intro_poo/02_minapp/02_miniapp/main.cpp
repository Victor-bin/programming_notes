#include <stdio.h>
#include <stdlib.h>

#include "cpersona.h"

int main (int argc, char *argv[]) {
    CPersona persona (34U, "Pepe");

    persona.display();

    return EXIT_SUCCESS;
}
