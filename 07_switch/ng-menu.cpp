#include <stdio.h>
#include <stdlib.h>

enum TOpcion { suma, resta, multiplicacion, division, TOTALOPCIONES };

const char *opciones[] = {
    "Sumar",
    "Restar",
    "Multiplicar",
    "Dividir",
    NULL
};

int menu () {
    int opcion;
    system ("clear");
    system ("toilet -fpagga --metal CALCULATOR");
    printf("\n");
    printf("\n");
    system ("./intro.sh");
    printf("\n");
    printf("\n");
    system ("toilet -fpagga --gay OPCIONES");

    for (int op=0;op<TOTALOPCIONES;op++)
    printf ("        %i.- %s. \n",op+1,opciones[op]);

    printf ("Tu opcion: ");
    scanf (" %i", &opcion);

    return opcion - 1;
}

int main () {
    int op1 = 7, op2 = 5;
    int opcion = menu ();

    printf ("La opción elegida es: %s\n", opciones[opcion]);

    switch (opcion) {
        case suma:
            printf ( "%i + %i = %2i\n", op1, op2, op1 + op2 );
            break;
        case resta:
            printf ( "%i - %i = %2i\n", op1, op2, op1 - op2 );
            break;
         case multiplicacion:
            printf ( "%i * %i = %2i\n", op1, op2, op1 * op2 );
            break;
       case division:
            printf ( "%i / %i = %2i , resto: %i\n", op1, op2, op1 / op2, op1 % op2 );
            break;

        default:
            fprintf (stderr, "Las opciones son del 1 al 4, vuelve a probar. \n");


            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
