#include <stdio.h>
#include <stdlib.h>

#define EXIT_SUCESS 0
#define NCARTAS 12
#define PICA 1
#define TREBOL 2
#define CORAZON 4
#define DIAMANTE 8

int pintapalo (int apintarf,int palo, char signo[20]) {

    int sapintarf;

    if (apintarf/palo==1){
        sapintarf = apintarf-palo;
        printf("CARTAS tipo %s :\n",signo);
        for (int carta=1;carta<=NCARTAS;carta++){
            printf(" %2i  %s  ",carta, signo);
            printf("\n");
        }
    }
    return sapintarf;
}

int pidepalo (int apintar,int palo, char signo[20]) {

    char caracter;

    printf("¿Quieres pintar las cartas del palo %s  (y/n)?",signo);
    scanf(" %c", &caracter);
    if (caracter == 'y' | caracter== 'Y')
        apintar= apintar + palo;

    return apintar;
}

int main (int argc, char *argv[]) {

    int apintar = 0;
    char pica[20] ="\u2660";
    char trebol[20]="\u2663";
    char corazon[20]="\u2665";
    char diamante[20]="\u2666";

    apintar = pidepalo(apintar,DIAMANTE,diamante);
    apintar = pidepalo(apintar,CORAZON,corazon);
    apintar = pidepalo(apintar,TREBOL,trebol);
    apintar = pidepalo(apintar,PICA,pica);

    apintar = pintapalo(apintar,DIAMANTE,diamante);
    apintar = pintapalo(apintar,CORAZON,corazon);
    apintar = pintapalo(apintar,TREBOL,trebol);
    apintar = pintapalo(apintar,PICA,pica);

    return EXIT_SUCCESS;
}
