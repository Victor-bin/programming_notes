
#include <stdio.h>
#include <stdlib.h>

#define EXIT_SUCESS 0

int main (int argc, char *argv[]) {

    int base;
    char caracter;


    printf("Introduce el caracter que formara el cuadrado:");
    scanf(" %1c", &caracter);


    printf("Introduce el numero de %c a pintar:", caracter);
    scanf(" %1i", &base);

    for (int fila=0;fila<base;fila++){
        for (int col=0;col<base;col++)
          printf(" %c ", caracter);

        printf("\n");

    }

    return EXIT_SUCCESS;
}
