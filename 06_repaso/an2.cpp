
#include <stdio.h>
#include <stdlib.h>

#define EXIT_SUCESS 0

int main (int argc, char *argv[]) {

    int base;

    printf("Introduce el numero de asteriscos a a pintar:");

    scanf(" %i", &base);

    for (int i=0;i<base;i++)
        printf("*");

    printf("\n");

    return EXIT_SUCCESS;
}
