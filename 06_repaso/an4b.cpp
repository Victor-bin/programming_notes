
#include <stdio.h>
#include <stdlib.h>

#define EXIT_SUCESS 0

const char *simbolo = "^ ";

int main (int argc, char *argv[]) {

    int base;

    printf("Introduce el numero de filas a pintar:");
    scanf(" %i", &base);

    for (int fila=0;fila<base;fila++){
        for (int esp=0;esp<(base-fila);esp++)
            printf(" ");
        for (int col=0;col<=fila;col++)
            printf("%s", simbolo);

        printf("\n");

    }

    return EXIT_SUCCESS;
}
