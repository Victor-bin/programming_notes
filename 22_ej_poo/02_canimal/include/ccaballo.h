#ifndef __CCABALLO_H__
#define __CCABALLO_H__

#include "cmamifero.h"

enum TCaballo {horse, pony};

class CCaballo:public CMamifero {
    public:
        bool is_shoeing ();
        void set_horseshoe (bool shoeing);
        TCaballo get_type ();
        CCaballo (double c_temp, unsigned c_extremities, unsigned age, bool shoeing);
        CCaballo (TCaballo type, double c_temp, unsigned c_extremities, unsigned age, bool shoeing);
        void gallop ();
        void walk () override;

        ~CCaballo();
    private:
        bool shoeing_;
        TCaballo type;

};

#endif
