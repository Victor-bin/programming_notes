#ifndef __CMAMIFERO_H__
#define __CMAMIFERO_H__

enum TScales {Celsius, Fahrenheit, Reaumur, Roemer, Newton, Deisle, Kelvin, Rankine};

class CMamifero
{
    public:
        unsigned get_extremities() const;
        double   get_temp(TScales scale) const;
        unsigned get_age();
        virtual void walk() = 0;
        void move();
        void move(int x, int y);

    protected:
        CMamifero ();
        CMamifero (double c_temp, unsigned c_extremities, unsigned age);

    private:
        unsigned c_extremities_;
        unsigned age;
        double   c_temp_;
};

#endif
