#ifndef __CENTERRADOR_H__
#define __CENTERRADOR_H__

#include "ccaballo.h"
#include "chumano.h"

class CEnterrador {
    public:
        void bury(CHumano*);
        void bury(CCaballo*);
};

#endif
