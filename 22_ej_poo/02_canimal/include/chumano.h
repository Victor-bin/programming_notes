#ifndef __CHUMANO_H__
#define __CHUMANO_H__

#include "cmamifero.h"

class CHumano:public CMamifero
{
    public:
        CHumano (double c_temp, unsigned c_extremities, unsigned age) {};
        void greet ();
        void walk () override;

        ~CHumano();
};

#endif
