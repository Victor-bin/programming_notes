/*
 * =====================================================================================
 *
 *       Filename:  chumano.cpp
 *
 *    Description: class for humans
 *
 *        Version:  1.0
 *        Created:  06/06/20 12:36:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  victor-bin / VictorTi (), victortg00@gmail.com
 *   Organization:  Victor-bin
 *
 * =====================================================================================
 */


#include "chumano.h"
#include "centerrador.h"
#include <iostream>

using namespace std;

CHumano::~CHumano()
{
    CEnterrador().bury(this);
}

void
CHumano::greet ()
{
    cout << "Hey. \n";
}

void
CHumano::walk ()
{
    cout << "I walk calmly as a human\n";
}
