/*
 * =====================================================================================
 *
 *       Filename:  ccaballo.cpp
 *
 *    Description: class for horses
 *
 *        Version:  1.0
 *        Created:  07/06/20 09:38:17
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  victor-bin / VictorTi (), victortg00@gmail.com
 *   Organization:  Victor-bin
 *
 * =====================================================================================
 */


#include "ccaballo.h"
#include "centerrador.h"
#include <iostream>

using namespace std;

CCaballo::CCaballo (double c_temp, unsigned c_extremities, unsigned age, bool shoeing): CMamifero(c_temp, c_extremities, age)
{
    type = horse;
    shoeing_ = shoeing;
}

CCaballo::CCaballo (TCaballo type, double c_temp, unsigned c_extremities, unsigned age, bool shoeing): CMamifero(c_temp, c_extremities, age)
{
    this->type = type;
    shoeing_ = shoeing;
}

CCaballo::~CCaballo()
{
    CEnterrador().bury(this);
}

bool
CCaballo::is_shoeing ()
{
    return shoeing_;
}

void
CCaballo::set_horseshoe (bool shoeing)
{
    shoeing_ = shoeing;
}

TCaballo
CCaballo::get_type()
{
    return type;
}

void
CCaballo::gallop ()
{
    cout << "I gallop as a horse\n";
}

void
CCaballo::walk ()
{
    cout << "I walk as a horse\n";
}
