/*
 * =====================================================================================
 *
 *       Filename:  cmamifero.cpp
 *
 *    Description: not instantiable class for mammals
 *
 *        Version:  1.0
 *        Created:  06/06/20 11:21:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  victor-bin / VictorTi (), victortg00@gmail.com
 *   Organization:  Victor-bin
 *
 * =====================================================================================
 */


#include "cmamifero.h"
#include <iostream>

using namespace std;

CMamifero::CMamifero ():CMamifero (23,4,0)
{
}

CMamifero::CMamifero (double c_temp, unsigned c_extremities, unsigned age){
    c_temp_ = c_temp;
    c_extremities_ = c_extremities;
    this->age = age;
}

unsigned
CMamifero::get_extremities() const
{
    return c_extremities_;
}

double
CMamifero::get_temp(TScales scale) const
{
    switch(scale)
    {
        case Fahrenheit:
            return (c_temp_ * 1.8) + 32;

        case Reaumur:
            return c_temp_ / 1.25;

        case Roemer:
            return c_temp_ * .525 + 7.5;

        case Newton:
            return c_temp_ * .33;

        case Deisle:
            return (100 - c_temp_) * 1.5;

        case Kelvin:
            return c_temp_ + 273.15;

        case Rankine:
            return c_temp_ * 1.8 + 491.67;

            // Celsius as default
        default:
            return c_temp_;
    }
}

unsigned
CMamifero::get_age()
{
    return age;
}

void
CMamifero::move()
{
    cout << "go to 0, 0\n";
}

void
CMamifero::move(int x, int y)
{
    cout << "go to " << x << ", " << y << "\n";
}
