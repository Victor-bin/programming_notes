/*
 * =====================================================================================
 *
 *       Filename:  centerrador.cpp
 *
 *    Description: Destroyer class
 *
 *        Version:  1.0
 *        Created:  19/06/20 09:55:55
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  victor-bin / VictorTi (), victortg00@gmail.com
 *   Organization:  Victor-bin
 *
 * =====================================================================================
 */


#include "centerrador.h"
#include <iostream>

using namespace std;

void
CEnterrador::bury(CHumano*){
    cout << "I have buried a person\n";
}

void
CEnterrador::bury(CCaballo*){
    cout << "I have buried a horse\n";
}

