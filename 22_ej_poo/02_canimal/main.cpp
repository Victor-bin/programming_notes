#include <stdio.h>
#include <stdlib.h>

#include "chumano.h"
#include "ccaballo.h"

int main (int argc, char *argv[]) {

    CHumano h  ( 23, 4, 10 );
    CCaballo e ( 24, 4, 20, true );
    CCaballo p ( pony, 32, 4, 2, false );

    h.move();
    h.move( 1, 10 );
    h.greet();
    p.get_type();
    e.get_type();
    h.walk();
    e.walk();

    return EXIT_SUCCESS;
}
