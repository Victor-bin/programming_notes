#include <stdio.h>
#include <stdlib.h>
#include  "cpersona.h"

#define MAX 10

const char * nombre[MAX + 1] = {
    "Pepe",
    "Alfonso",
    "Maria",
    "Mario",
    "Pepa",
    "Miguel",
    "Marta",
    "Antonio",
    "Sara",
    "Sonia",
    NULL
};

int main (int argc, char *argv[]) {
    CPersona persona[MAX];

    for(int i = 0; i < MAX; i++)
        persona[i] = CPersona(nombre[i]);


    for(int i = 0; i < MAX; i++)
        persona[i].saluda();

    return EXIT_SUCCESS;
}
