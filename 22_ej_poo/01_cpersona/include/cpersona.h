#ifndef __CPERSONA_H__
#define __CPERSONA_H__

class CPersona {
    char nombre[20];
    const char *apellido = "exposito";

    public:
    CPersona();
    CPersona (const char *nombre);
    void saluda();
};

#endif
