#include "cpersona.h"
#include <iostream>
#include <string.h>

CPersona::CPersona (){};

CPersona::CPersona (const char *nombre)
{
    strcpy(this->nombre,  nombre);
}

void
CPersona::saluda() {
    std::cout << "Hola, " << this->nombre << " " << this->apellido << std::endl;
}

