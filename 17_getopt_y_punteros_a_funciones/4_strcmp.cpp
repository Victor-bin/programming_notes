#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PALABRAS 4

void compara(const char *lista[], int a, int b){
    if( strcmp (lista[a], lista[b]) > 0  ){
        const char *aux = lista[a]; //esto no es c estandar
        lista[a] = lista[b];
        lista[b] = aux;
    }

}

int main (int argc, char *argv[]) {
    const char *lista[PALABRAS]= {
        "Pepe",
        "Pepa",
        "Ana",
        "Marcos"
    };

    for (int n = 0; n < PALABRAS;n++)
        for (int i = 0; i < PALABRAS - 1; i++)
            compara(lista,i, i + 1);

    for (int i = 0; i < PALABRAS; i++)
        printf("%s\n",lista[i]);


    return EXIT_SUCCESS;
}
