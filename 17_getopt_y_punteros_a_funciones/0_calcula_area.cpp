#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

enum TOpc{rec, tri} opc = rec;

const char *program_name;

void uso(FILE *salida){
    int error = 0;
    if (salida == stderr)
        error = 1;

    fprintf(salida,"%s\n\
            Calcula el area\n\
            parametros:\n\
            \t-h muestra ayuda\n\
            \t-t Calcula el triangulo\n\
            \t-r Calcula el rectangulo(opcion por defecto)\n",program_name);
    exit(error);
}

double rectangulo(double base, double altura){return base * altura;}
double triangulo (double base, double altura){return base * altura / 2;}

int main (int argc, char *argv[]) {
    char c;
    double base, altura, resultado;
    double (*operacion[2]) (double, double) = {&rectangulo, &triangulo};

    program_name = argv[0];

    while((c = getopt (argc, argv, "htr")) != -1){
        switch(c){
            case 'h': uso(stdout);
                      break;
            case 't': opc = tri;
                      break;
            case 'r':break;
        }
    }

    printf("introduce la base:\t");
    scanf("%lf",&base);
    printf("introduce la altura:\t");
    scanf("%lf",&altura);

    resultado = (*operacion[opc]) (base, altura);
    printf("El resultado con base: %.2lf y altura: %.2lf  es %.2lf \n",base,altura, resultado);

    return EXIT_SUCCESS;
}
