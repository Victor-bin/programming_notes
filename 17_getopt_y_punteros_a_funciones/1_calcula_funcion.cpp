#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

enum TOpc{funs, funf} opc = funs;

const char *program_name;

void uso(FILE *salida){
    int error = 0;
    if (salida == stderr)
        error = 1;

    fprintf(salida,"%s\n\
            Calcula la funcion\n\
            parametros:\n\
            \t-h muestra ayuda\n\
            \t-f Calcula x²\n\
            \t-s Calcula x³/2(opcion por defecto)\n",program_name);
    exit(error);
}

double f(double x){return x * x;}
double s(double x){return x * x * x / 2;}

int main (int argc, char *argv[]) {
    char c;
    double x, resultado;
    double (*funcion[2]) (double) = {&s, &f};

    program_name = argv[0];

    while((c = getopt (argc, argv, "hfs")) != -1){
        switch(c){
            case 'h': uso(stdout);
                      break;
            case 'f': opc = funf;
                      break;
            case 's':break;
        }
    }

    printf("introduce la x:\t");
    scanf("%lf",&x);

    resultado = (*funcion[opc]) (x);
    printf("El resultado con x=%.2lf  es %.2lf \n", x, resultado);

    return EXIT_SUCCESS;
}
