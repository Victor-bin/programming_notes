# Apuntes de trinchera
## Tokens
Existen los diferentes tokens
- palabras clave
- identificadores
- constantes
- cadenas de caracteres
- comentarios
- signos de puntuacion
- operadores
- separadores

# Palabras claves
Las palabras claves de C son:
- auto: tipo de variable (atomatico)
- break: salida de bucles
- case:  compara variable y valor
- char: reserva en memoria 1 Byte
- const: variable con valor congelado
- continue: otra vuelta al bucle
- default: si no case con nada hace default
- do: hacer mientras (siempre con while)
- double: tipo de dato (numero reales)
- else: sino (siempre con if)
- enum: definir constantes (asignar alias)
- extern: decir que la variable es de otro fichero
- float: tipo de datos (menos precision de double, no se usa)
- for: Bucle durante un numero concido
- goto: salto de linea (no utilizado, orden prohibida)
- if: si, se sumple mientas la condicion sea correcta
- int: tido de datos para numeros enteros
- long: modificador de tipo de dato, duplica su capacidad
- register: modificador de tipo de datos, si puede guarda la variable en el micro (global)
- return: termina una funcion
- short: modificador de datos, divide la longitud
- signed: Tipo de dato que admite negativos
- sizeof: operador qeu define cuanto ocupa una variable o tipo de dato
- static: para mantener las variables locales vivas (no las globaliza)
- struct: variable compuesta por por variables (similar a objetos)
- switch: mira el valor de una variable y aplica default o case
- typedef: crea un alias  para tipos de datos
- union: vale para meter dos variables en la misma celda (no muy util)
- unsigned: modificador de los tipos de datos, todos los seran posivos
- void: Tipo de dato, dato vacio
- volatile: Modificador de tipo de datos (volatil), la variable puede cambiar fura del programa
- while: mientras, comrueba una condicion

> La directivas del preprocedador (COMO LAMADAS) Se escriben en m4, lenguaje de macros (en c empiezan con #)

## Guia de estilo:
- Identificadores en minuscula si refieren a una posicion de memoria
- Los nombres deben ser descritivos, signiticativos y identificativos
- Si empiezan en minusculas solo llevan minusculas (el espacio sera _ ) (notacion underscore)
- Los tipos de datos empiezan por mayusculas (notacion de camello)
- Las constantes iran totamente en mayusculas
- Las varibles distingen mayusculas y minusculas

