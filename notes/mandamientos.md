# Mandamientos del programador

1. Entender el enunciado
2. Hacer el programa a mano
3. Abstraer el problema
4. Codificar
5. Seguimiento
