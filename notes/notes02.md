#Apuntes de trinchera

Dado una puntero y una array que contienen "cadenas" vemos que:

- const char \* const cprog --> sizeoft(vprog) --> 4(tamaño puntero)
- char vprog[] --> sizeoft(vprog) --> 8(tamaño array)
