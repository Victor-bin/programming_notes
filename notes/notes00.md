# Apuntes de trinchera
Cuando se guardan datos en memoria en orden inverso se dice que estan en Puntero fino (little endian) y en el caso contrario en puentero grueso (big endian)

Char  = 8   bits o 1 byte
Int   = 16  bits o 4 bytes
float = numeros reales
double= numeros reales (float x2)

(LONG SHORT son modificadores de datos que duplican o dividen a la mitad el tamaño de una varible)
Aritmetica modular
: 256 = 0 ; 257 = 1; 510 = 255

Se pueden guardar numeros en memoria como Signed o Unsigned (si hay o no negativos)

## Ca2
- Sacar el negativo en binario o complemento a 2 (Ca2)
  - Se pasan los 0 a 1 y viceversa (Ca1) y se suma 1 (Ca2) asi el resultado de su suma da 0
  - O se busca el primer uno y de el a la deracha se queda como esta y a la izquierda se le aploca el (Ca1)

Si hay negativos ya no va del 0 al 255 sino del 127 al -128

## Guardar numeros reales (con decimales)

2,7358 = 0,27358 * 10 elevado a 1
0,00025414 = 25414 * 10 elevado a -3
lo que se guardaria como:


| mantisa | exponente|
| -- | -- |
| 27358 | 1 |
| 25414 | -3 |

## Sistema de pasos de parametros

convenciones de llamada

Formato de la pila del procesador
- ip -> Proxima instruccion (instrucction pointer) En los programas viejos se llama pc (program counter)
- bp -> Donde acaba la pila (base pointer)
- sp -> Ultima posicion ocupada (stack pointer)

La pila almacena:
- Direccion de retorno
- Parametros
- Variables locales

Un programa es la suma del algoritmo mas las varibles
(la pila siempre crece hacia abajo, o lo que es lo mismo, decrece)

call(cd) da lugar a las funciones(trozos de codigo que se repiten con frecuencia), necesitan de la pila (stack), parametros y dir de retorno.
jp (direccion exacta) / jr (direccion reletiva) son estructuras de control, pueden ser iterativas (bucles) o alternativas (condicionales)

    Una variable local solo pertenece a su propia funcion y solo duerante esa llamada
    Las variables tienen ambito (cantidad de tiempo que esta viva) y visibilidad (donde es conociada la variable)
    El valor de retorno se guarda en el microprocesador
    Para los lenguajes de programacion el upcasting es automatico/implicito pero el downcasting no
    (upcasting es trasformar de tipo pequeño a grande Ej: de char a int)
    (downcasting es transformar de tipo grande a pequeño Ej: de int a char)
    El acumulador guarda el resultado de la ultima operacion relizada por el micro

    Una expresion es un conjunto de constantes, operadores y/o variables

    Las constantes de tipo enumeración limitan el valor de una variable

    sizeof nos dice el n de bytes que ocupa una variable o constante



