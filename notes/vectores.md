# Vectores
## Caracteristicas
- Magnitud, longituz y direcion.
- Usa orden x,y,z = (1,3,5).
- Siempre empiezan en 0.
- Pueden ser negativos (SIGNED).
- Tiene una proyeccion por plano.

la proyecion sobre z de:
```math
 \vec{a}=(1,3,5)
 ```
seria :
```math
 \vec{a_{pro}}=(1,3,0)
 ```
- Estas proyecciones tienen proyecciones en ejes llamadas componentes.
- los componetes son:
    - i para la x
    - j para la y
    - k para la z

por ello

```math
 \vec{a}=(1,3,5)
 ```
 es equivalente a:
 ```math
 \vec{a}=(1\vec{i},3\vec{j},5\vec{k})
 ```

- Algunos vectores estan ligados a una recta para mantener sus propiedades.
- Tambien existen vectores fijos (los menos).
- Al resto se les llama libres.
- La medida de un vector es conocido como **modulo**


>  los vectores que miden uno se llamn unitarios, si un conjunto de estos forman noventa grados son llamados versores.

## Sumas
```math
 \vec{a}=(1,3,5)
 ```
 ```math
 \vec{b}=(0,2,-7)
  ```
  ```math
 \vec{c}=\vec{a}+\vec{b}=(1,5,-2)
 ```
## Modulos
Dado:
```math
 \vec{a}=(2\vec{i},1\vec{j}) == \vec{a}=(2,1)
 ```
 Calculamos:
```math
\left| \vec{a} \right| = \sqrt{x^2 + y^2} = \sqrt{2^2 + 1^2} = \sqrt{5}
```

> y = x \* tg alpha

> sen alpha = y / r

El sen es equivalente al valor de y cuando r = 1

Sen < tg

el sen es respecto a la elevacion(y) y la tangente respecto al plano(x)

el Cos es respecto a la elevacion(y) pero sobre el plano(x)

Cos^2 alpha + Sen^2 alpha = 1

tg alpha = sen alpha / cos alpha
