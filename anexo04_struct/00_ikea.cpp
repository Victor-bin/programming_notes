#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

struct Tmesa {
    int patas;
    char color[MAX];
} norbeg ;//modo declaracion 1 (global?)

struct Testante {
    int cosas;
    double peso;
} estante[5];// declara array de estructuras

int main (int argc, char *argv[]) {
    struct Tmesa lack;//modo declaracion 2 (local?)
    struct Tmesa *comprada;
    lack.patas = 4;
    strcpy(lack.color,"white");
    norbeg.patas = 1;
    strcpy(norbeg.color, "blak");

    //compramos la mesa lack
    comprada = &lack;

    //ver el color
    comprada -> color;
    // equivalente a
    (*comprada).color;

    return EXIT_SUCCESS;
}
