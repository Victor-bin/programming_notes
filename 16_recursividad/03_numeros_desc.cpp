#include <stdio.h>
#include <stdlib.h>

void printDesc( unsigned n){
    if (n == 0)
        printf("\t%u",n);

    else{
        printf("\t%u",n);
        printDesc(n-1);
    }
}

int main (int argc, char *argv[]) {

    unsigned num;

    printf("Voy a imprimir de N hasta 0 (numeros naturales)\n\t Valor de N: ");
    scanf("%u", &num);

    printDesc(num);
    printf("\n");

    return EXIT_SUCCESS;
}
