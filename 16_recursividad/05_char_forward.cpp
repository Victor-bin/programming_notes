#include <stdio.h>
#include <stdlib.h>

void printChar( const char * arr){
    if (*arr == '\0')
        printf("\t%c",*arr);

    else{
        printf("\t%c",*arr);
        printChar(arr + 1);
    }

    return;

}

int main (int argc, char *argv[]) {

    char phrase[10]= "Wow space";

    printChar(phrase);
    printf("\n");

    return EXIT_SUCCESS;
}
