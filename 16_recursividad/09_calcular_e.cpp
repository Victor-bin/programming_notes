#include <stdio.h>
#include <stdlib.h>

double factorial( int n){
    double result;
    if (n < 1)
        result = 1;

    else
        result = n * factorial(n - 1);

    return result;

}

double calcula_e(int n){
    double e;
    if (n < 1)
        e = 1 / factorial(n);
    else
        e = 1/ factorial(n) + calcula_e(n-1);

    return e;
}

int main (int argc, char *argv[]) {
    int num;
    double e;

    printf("Voy a calcular el numero e con N terminos\n\t Valor de N: ");
    scanf("%i", &num);

    e = calcula_e(num);
    printf("\ne(%i) es: %lf\n",num, e);

    return EXIT_SUCCESS;
}
