#include <stdio.h>
#include <stdlib.h>

unsigned sumaNNumeros(unsigned n){
    unsigned result;
    if (n == 1)
        result = n;

    else
        result = n + sumaNNumeros(n - 1);

    return result;

}

int main (int argc, char *argv[]) {

    unsigned num, suma;

    printf("Voy a calcular el la suma de los N numeros naturales\n\t Valor de N: ");
    scanf("%u", &num);

    suma = sumaNNumeros(num);
    printf("\nLa suma de los %u numeros naturales es: %u\n",num, suma);

    return EXIT_SUCCESS;
}
