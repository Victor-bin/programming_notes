#include <stdio.h>
#include <stdlib.h>

double factorizar( double n){
    double result;
    if (n == 1)
        result = n;

    else
        result = n * factorizar(n - 1);

    return result;

}

int main (int argc, char *argv[]) {

    double num, factorial;

    printf("Voy a calcular el factorial de N (numeros naturales)\n\t Valor de N: ");
    scanf("%lf", &num);

    factorial = factorizar(num);
    printf("\nEl factorial de %lf es: %lf\n",num, factorial);

    return EXIT_SUCCESS;
}
