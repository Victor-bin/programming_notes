#include <stdio.h>
#include <stdlib.h>

void printDesc( unsigned n){
    if (n == 0)
        printf("\t%u",n);

    else{
        printDesc(n-1);
        printf("\t%u",n);
    }
}

int main (int argc, char *argv[]) {

    unsigned num;

    printf("Voy a imprimir de 0 hasta N (numeros naturales)\n\t Valor de N: ");
    scanf("%u", &num);

    printDesc(num);
    printf("\n");

    return EXIT_SUCCESS;
}
