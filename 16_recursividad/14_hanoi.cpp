#include <stdio.h>
#include <stdlib.h>

#define N 5

enum TPalo {origen, intermedio, destino};

void hanoi(int n, int *cima_a, int a[N], int *cima_c, int c[N],int *cima_b, int b[N]){
    if (n > 0){
        hanoi(n - 1, cima_a, a, cima_b, b, cima_c, c);
        printf("mover %i(%i) a %i \n",*cima_a,a[*cima_a],*cima_c);
        c[(*cima_c)++] = a[--*cima_a] ;
        a[*cima_a] = 0;
        hanoi(n - 1, cima_b, b, cima_c, c, cima_a, a);
    }

}

int main (int argc, char *argv[]) {

    int palo[3][N];
    int cima[3]; // Indica la primera posición libre de cada palo.
    int *p_origen, *p_intermedio, *p_destino;

    p_origen = &palo[origen][0];
    p_intermedio = &palo[intermedio][0];
    p_destino = &palo[destino][0];


    for( int i = 0; i < N; i++)
        palo[origen][i] = N - i;
    cima[origen] = 5;
    cima[intermedio] = cima[destino] = 0;

    hanoi(N, &cima[origen], p_origen, &cima[destino], p_destino, &cima[intermedio], p_intermedio);

        for (int i = 0; i < N; i++)
            printf("%i \t",palo[destino][i]);
        printf("\n");


    return EXIT_SUCCESS;
}
