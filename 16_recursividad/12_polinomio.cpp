#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIF 0.01

unsigned pregunta_grado(){
    unsigned g;
    printf("¿De que grado es el polinomio que va a intoducir?  \t");
    scanf(" %u",&g);
    return g;
}

void pregunta_limite(double *li,double *ls){
    printf("¿Cual es el limite inferior?\t");
    scanf(" %lf",li);
    printf("¿Cual es el limite superior?\t");
    scanf(" %lf",ls);
}

double * crear(unsigned grado){
    return (double *) malloc( (grado + 1) * sizeof(double) );
}

void pregunta_polinomio(double *pol,unsigned  grado){
    printf("Introduce los datos del polinomio de grado %i (de forma ascendente)\n",grado);

    for (int i = 0; i <= grado; i++){
        for (int j = 0; j <= i; j++)
            printf("\t");

        scanf(" %lf",pol + i);
        printf("\x1b[%iA",1);
    }
    printf("\n");
    return;
}

double f(double *pol, unsigned grado, double x){
    double valor = 0;

    for(int i = 0; i <= grado; i++)
        valor += *(pol + i) * pow(x,i);

    return valor;
}

void cero(double *pol, unsigned grado, double li, double ls){
    double max, min,
           med = (li + ls) / 2;
    if (ls - li < DIF){
        printf("La raiz del polinomio se encuentra en : %.2lf\n", med );
        return;
    }

    max = f(pol, grado, ls);
    min = f(pol, grado, li);

    if((max >= 0 && min >= 0) || (max <  0 && min <  0)){
        cero(pol, grado, li, med);
        return;
    }

    cero(pol, grado, med, ls);
    return;
}

int main (int argc, char *argv[]) {
    double *pol, li, ls;
    unsigned g = pregunta_grado();
    pol = crear(g);
    pregunta_polinomio(pol, g);
    pregunta_limite(&li, &ls);

    cero(pol, g, li, ls);
    free(pol);

    return EXIT_SUCCESS;
}
