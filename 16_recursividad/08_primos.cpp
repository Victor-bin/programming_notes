#include <stdio.h>
#include <stdlib.h>


int numero_de_divisores(int n){
    static int div = n - 1;
    int divisores = 0;
    if (div > 1 ){
        if (!(n % div))
        divisores++;

        div--;
        divisores += numero_de_divisores(n);
    }
    return divisores;
}

bool es_primo(int n){

    if (numero_de_divisores(n) < 1)
        return true;
    else
        return false;
}

int main (int argc, char *argv[]) {

    unsigned num;

    printf("Voy a calcular si N es primo (numeros naturales)\n\t Valor de N: ");
    scanf("%u", &num);

    printf( es_primo(num)? "El numero %i es primo\n":"El numero %i no es primo\n",num);

    return EXIT_SUCCESS;
}
