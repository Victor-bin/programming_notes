#include <stdio.h>
#include <stdlib.h>

const char *ordinal[3][10] = {
    { "", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "séptimo", "octavo", "noveno" },
    { "", "décimo", "vigésimo", "trigésimo", "cuatrigésimo", "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo", "nonagésimo" },
    { "", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" }
};

void di_ordinal(int num,int vuelta){
    int div, res;

    div = num / 10;
    res = num % 10;
    if (div >=1)
        di_ordinal(div, vuelta + 1);

    printf("%s ", ordinal[vuelta][res]);

}

int main (int argc, char *argv[]) {
    int num;

    printf("Vamos a necesitar un numero:\t");
    scanf("%i",&num);

    di_ordinal(num,0);
    printf("\n");


    return EXIT_SUCCESS;
}
