#include <stdio.h>
#include <stdlib.h>

#define DIF 0.00001

double factorial( int n){
    double result;
    if (n < 1)
        result = 1;

    else
        result = n * factorial(n - 1);

    return result;

}

double calcula_e(int n){
    double e = 1 / factorial(n);
        if (e  > DIF)
            e += calcula_e(n+1);

    return e;
}

int main (int argc, char *argv[]) {
    double e ,diferencia;
    int num = 0;

    printf("Voy a calcular el numero e \n\t");

    e = calcula_e(num);

    printf("\ne es: %lf\n", e);

    return EXIT_SUCCESS;
}
