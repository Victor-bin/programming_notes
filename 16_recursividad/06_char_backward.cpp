#include <stdio.h>
#include <stdlib.h>

void printChar( const char * arr){
    if (*arr == '\0')
        printf("\t%c",*arr);

    else{
        printChar(arr + 1);
        printf("\t%c",*arr);
    }
}

int main (int argc, char *argv[]) {

    char phrase[10]= "Wow space";

    printChar(phrase);
    printf("\n");

    return EXIT_SUCCESS;
}
