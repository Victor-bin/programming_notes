#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int factorial( int n){
    int result;
    if (n == 1)
        result = n;

    else
        result = n * factorial(n - 1);

    return result;

}

int function(int n, int x){
    int result;

    if (n > 1)
    result = pow(-1,n) * n / factorial(n) + function(n-1);
    else
        result = 1;

    return result;
}

int main (int argc, char *argv[]) {

    int n,x,res;

    printf("Voy a calcular S(n) = (-1)^n * x^n / n!\n\t Valor de N: ");
    scanf("%i", &n);
    printf("\n\t Valor de X: ");
    scanf("%i", &x);

    res = function(n,x);

    printf("S(%i),x = %i es %i \n",n,x,res);

    return EXIT_SUCCESS;
}

