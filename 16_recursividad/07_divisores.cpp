#include <stdio.h>
#include <stdlib.h>


void printDiv( unsigned n){
    static int div = n;
    if (div > 1 ){
        if (!(n % div))
        printf("\t%u", div);

        div--;
        printDiv(n);
    }
    return;
}

int main (int argc, char *argv[]) {

    unsigned num;

    printf("Voy a imprimir los divisores de N (numeros naturales)\n\t Valor de N: ");
    scanf("%u", &num);

    printDiv(num);
    printf("\n");

    return EXIT_SUCCESS;
}
