#include <stdio.h>
#include <stdlib.h>

/*
 * This program is a decimal converter to Hexadecimal and Octal
 */

int main () {

    //Definition
    int number; // this variable is used to save a number

    // First line
    printf("Enter as integer: \n");
    scanf("%d", &number); // Enter the data
  Oprintf("The number %d is %x in Hexadecimal and %o in Octal. \n", number , number, number);// print the data in base 8 and 16

    return 0;
};
